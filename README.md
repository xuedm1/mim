# Reference Software MIM for AVS 3-D Medical Image Coding Standard


This software package is the reference software for AVS 3-D Medical Image Coding Standard. The reference software includes both encoder and decoder functionality.
Reference software is useful in aiding users of the coding standard to establish and test conformance and interoperability, and to educate users and demonstrate the capabilities of the standard. For these purposes, this software is provided as an aid for the study and implementation of 3-D medical image coding.
The software has been developed by the AVS Video Coding Working Group.


# Datasets preparation

You can download the training and testing datasets via the following pages: 

For CT dataset:

Training: https://www.kaggle.com/datasets/andrewmvd/mosmed-covid19-ct-scans?resource=download&select=CT-2

Testing: https://www.kaggle.com/datasets/andrewmvd/mosmed-covid19-ct-scans?resource=download&select=CT-3

For MRI dataset:

Training: https://www.kaggle.com/competitions/trabit2019-imaging-biomarkers/data?select=train.zips

Testing: https://www.kaggle.com/competitions/trabit2019-imaging-biomarkers/data


# Common Training and Testing condition


To align the target bitrate, we fix a set of scale values. 

For MRI, the scale values are {24, 14, 8.5, 6.5, 4}; 

For CT, the scale values are {36, 20, 12, 8.5, 6}.


# Training instructions


This software does not suppert running on pure CPU machines now.
Open a command prompt on your system and change into the root directory of this project.

Take CT for example, to train from scretch, first:

```sh
  cd ./train_ck_4_new
```
Open train_lossless_ct.py and change the command line arguments to modify paths, etc:

```sh
parser.add_argument('--input_dir', type=str, default='/data')
parser.add_argument('--out_dir', type=str, default='/checkpoint')
parser.add_argument('--log_dir', type=str, default='/output')
```


 Then:
```sh
  cd ./sh
  sh ./sh/train_ct_lossless.sh
```
For lossy coding, to train from scretch simply call:

```sh
  sh ./sh/train_ct_lossy.sh
```

# Encode and decode instructions

Open the cfg file to configure the path and other information.

To encode a file simply call:

```sh
  sh ./sh/encode.sh
```

To decode a bitstream simply call:

```sh
  sh ./sh/decode.sh
```


<!-- # Training instructions


All models is stored in the model directory. You could run the code in ./source/train directory to get your own model.
More details could be found in doc directory. -->



# Contributing

This project welcomes contributions and suggestions.

# Contributors

Dongmei Xue, xdm1@mail.ustc.edu.cn

