from Util.MyData_test_new import MyData
from torch.utils.data import DataLoader
import SimpleITK as sitk
from Model.Model_mri_lossy import Model
# from Model.Model_mri_uint import Model
import argparse
import torch
from torch.autograd import Variable
import os
import glob

parser = argparse.ArgumentParser(description='.')

# parameters
parser.add_argument('--input_dir', type=str, default='/data/xiaoban/cfe/test_data/MRI')
parser.add_argument('--log_dir', type=str, default='/data/yuyue/aiwave_train3/test')
parser.add_argument('--load_model', type=str, default='/data/yuyue/aiwave_train3/mri04_uint03/checkpoint/mri_sign_lossy_24/mri_sign_lossy/model_epoch01760.pth')
parser.add_argument('--epochs', type=int, default=300000)
parser.add_argument('--batch_size', type=int, default=1)
parser.add_argument('--init_scale', type=float, default=24)
parser.add_argument('--main_model', type=str, default=None)


def to_variable(x):
    if torch.cuda.is_available():
        x = x.cuda()
    return Variable(x)


def save_nii(img, path):
    img = sitk.GetImageFromArray(img)
    sitk.WriteImage(img, path)


def main():
    args = parser.parse_args()

    if not os.path.exists(args.log_dir):
        os.makedirs(args.log_dir)
    logfile = open(args.log_dir + '/testmri.txt', 'w')

    if args.load_model is not None:
        checkpoint = torch.load(args.load_model)
        model = Model(wavelet_affine = False)
        # state_dict = checkpoint['state_dict']
        state_dict = {k.replace('module.', ''): v for k, v in checkpoint['state_dict'].items()}
        model.load_state_dict(state_dict)
        print('Load pre-trained model [' + args.load_model + '] succeed!')
        logfile.write('Load pre-trained model [' + args.load_model + '] succeed!' + '\n')
        logfile.flush()

    if torch.cuda.is_available():
        if torch.cuda.device_count() > 1:
            print("Let's use", torch.cuda.device_count(), "GPUs!")
            model = torch.nn.DataParallel(model).cuda()
        else:
            model = model.cuda()

    logfile.write('Load data starting...' + '\n')
    logfile.flush()

    logfile.write('Load all data succeed!' + '\n')
    logfile.flush()

    test_files = glob.glob(os.path.join(args.input_dir, '*.nii'))
    test_files.sort()


    # test_high_files = glob.glob(os.path.join(args.input_high_dir, '*.npy'))
    # test_high_files.sort()
    DS = MyData(files=test_files)
    print("Number of testing images: ", len(DS))
    test_loader = DataLoader(dataset=DS, batch_size=args.batch_size, shuffle=False, drop_last=True)
    max_step = test_loader.__len__()
    print(max_step)

    psnr_all = 0.
    bpp_all = 0.

    store_path = f'/data/yuyue/aiwave_train3/test_sub_sign/MRI_{args.init_scale}/'

    # if not os.path.exists(store_path):
    #     os.makedirs(store_path + '/high/')
    #     os.makedirs(store_path + '/sign/')

    for batch_idx, input_img_ in enumerate(test_loader):
        input_img_v = to_variable(input_img_)
        size = input_img_v.size()
        print(size)


        
        with torch.no_grad():
            # mse, bpp, sacle, recon = model(input_img_v, args.init_scale, train=0, wavelet_trainable=0)
            mse, bpp, sacle, recon, hhh, sss = model(input_img_v, args.init_scale, train=0, wavelet_trainable=0)

        # import pdb
        # pdb.set_trace()

        # for i in range(8):
        #     for j in range(4):
        #         if i == 0 and j in [0, 1, 2]:
        #             continue
        #         name = test_files[batch_idx].split('/')[-1].split('.')[0]
        #         save_nii(hhh[i][j].cpu(), f'{store_path}/high/{name}_{i}{j}.nii')
        #         save_nii(sss[i][j].cpu(), f'{store_path}/sign/{name}_{i}{j}.nii')

        mse = torch.mean(mse)
        psnr = 10. * torch.log10(65535. * 65535. / mse)

        bpp = torch.sum(bpp)
        size = input_img_v.size()
        num_pixels = size[0] * size[2] * size[3] * size[4]
        bpp = bpp / num_pixels

        psnr_all = psnr_all + psnr.item()
        bpp_all = bpp_all + bpp.item()

        print('image ID:', batch_idx)
        print('psnr', psnr.item())
        print('bpp', bpp.item())

        # exit(1)


        logfile.write('psnr:' + str(psnr.item()))
        logfile.write('bpp:' + str(bpp.item()))
        logfile.flush()
    print('psnr_mean: ' + str(psnr_all / max_step))
    print('bpp_mean: ' + str(bpp_all / max_step))

    logfile.write('psnr_mean:' + str(psnr_all))
    logfile.write('bpp_mean:' + str(bpp_all))
    logfile.flush()
    logfile.close()


if __name__ == "__main__":
    main()
