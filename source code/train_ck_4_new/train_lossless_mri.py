import os
import argparse
import glob
from Util.MyData import MyData
from torch.utils.data import DataLoader
import torch.optim as optim
import torch
import random
from torch.autograd import Variable
from tensorboardX import SummaryWriter
from Model.Model_lossless_mri import Model

parser = argparse.ArgumentParser(description='.')

# parameters
parser.add_argument('--input_dir', type=str, default='/data/xiaoban/cfe/MRI')
parser.add_argument('--out_dir', type=str, default='/data/xiaoban/multi-medical/checkpoint')
parser.add_argument('--log_dir', type=str, default='/output/mri')
parser.add_argument('--epochs', type=int, default=300000)
parser.add_argument('--scale_list', type=float, nargs="+", default=[10, 12, 14, 16, 18, 20, 24])
parser.add_argument('--lambda_value', type=float, default=8.0)
parser.add_argument('--batch_size', type=int, default=4)
parser.add_argument('--patch_sizeh', type=int, default=128)
parser.add_argument('--patch_sizew', type=int, default=128)
parser.add_argument('--patch_sizez', type=int, default=32)
parser.add_argument('--load_model', type=str, default=None)
parser.add_argument('--main_model', type=str, default=None)
parser.add_argument('--wavelet_affine', type=str, default=False)


def to_variable(x):
    if torch.cuda.is_available():
        x = x.cuda()
    return Variable(x)


def main():
    tb_logger = SummaryWriter('/output/')
    args = parser.parse_args()
    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)
    ckpt_dir = args.out_dir + '/mri'
    if not os.path.exists(ckpt_dir):
        os.makedirs(ckpt_dir)
    if not os.path.exists(args.log_dir):
        os.makedirs(args.log_dir)
    logfile = open(args.log_dir + '/mri.txt', 'a+')
    total_epoch = args.epochs

    if args.load_model is not None:
        checkpoint = torch.load(args.load_model)
        model = Model(wavelet_affine = False)
        state_dict = checkpoint['state_dict']
        state_dict_new = {}
        for k,v in state_dict.items():
            new_k = k.replace('module.', '') if 'module.' in k else keys
            state_dict_new[new_k] = v

        model.load_state_dict(state_dict_new)
        print('Load pre-trained model [' + args.load_model + '] succeed!')
        logfile.write('Load pre-trained model [' + args.load_model + '] succeed!' + '\n')
        logfile.flush()
        epoch = 1
    else:
        model = Model(wavelet_affine=False)
        epoch = 1

    if torch.cuda.is_available():
        if torch.cuda.device_count() > 1:
            print("Let's use", torch.cuda.device_count(), "GPUs!")
            model = torch.nn.DataParallel(model).cuda()
        else:
            model = model.cuda()

    logfile.write('Load data starting...' + '\n')
    logfile.flush()
    logfile.write('Load all data succeed!' + '\n')
    logfile.flush()

    train_files = glob.glob(os.path.join(args.input_dir, '*.nii'))
    DS = MyData(train_files, 1234, args.patch_sizeh, args.patch_sizew, args.patch_sizez)
    print("Number of training images: ", len(DS))
    train_loader = DataLoader(dataset=DS, batch_size=args.batch_size, shuffle=True, drop_last=True)
    max_step = train_loader.__len__()
    opt_bpp = optim.Adam(model.parameters(), lr=1e-4)


    while True:
        if epoch > total_epoch:
            break
        model.train()
        psnr_all = 0.
        bpp_all = 0.

        for batch_idx, input_img in enumerate(train_loader):

            input_img_v = to_variable(input_img)

            mse, bpp, recon = model(input_img_v, train=1, wavelet_trainable=0)
            mse = torch.mean(mse)
            bpp = torch.sum(bpp)
            size = input_img_v.size()
            bpp = bpp / size[0] / size[2] / size[3] / size[4]
            psnr = 10. * torch.log10(65535. * 65535. / mse)
            loss = bpp

            opt_bpp.zero_grad()
            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), 5.0, norm_type=2)
            opt_bpp.step()

            psnr_all = psnr_all + psnr.item()
            bpp_all = bpp_all + bpp.item()

            if batch_idx % 1 == 0:
                logfile.write('Train Epoch: [' + str(epoch) + '/' + str(total_epoch) + ']   ' + 'Step: [' + str(
                    batch_idx) + '/' + str(max_step) + ']   '
                              + 'train loss: ' + str(psnr.item()) + '/' + str(bpp.item()) + '\n')
                logfile.flush()
                print('Train Epoch: [' + str(epoch) + '/' + str(total_epoch) + ']   ' + 'Step: [' + str(
                    batch_idx) + '/' + str(max_step) + ']   '
                      + 'train loss: ' + str(psnr.item()) + '/' + str(bpp.item()))

        if epoch % 1 == 0:
            logfile.write('psnr_mean: ' + str(psnr_all / max_step) + '\n')
            logfile.write('bpp_mean: ' + str(bpp_all / max_step) + '\n')
            logfile.flush()
            print('psnr_mean: ' + str(psnr_all / max_step))
            print('bpp_mean: ' + str(bpp_all / max_step))

            tb_logger.add_scalar('psnr', psnr_all / max_step, epoch)
            tb_logger.add_scalar('psnr', bpp_all / max_step, epoch)

        if epoch % 100 == 0:
            torch.save({'epoch': epoch, 'state_dict': model.state_dict()},
                       ckpt_dir + '/model_epoch' + str(epoch).zfill(5) + '.pth')

        epoch = epoch + 1
    logfile.close()


if __name__ == "__main__":
    main()