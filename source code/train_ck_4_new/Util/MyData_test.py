import torch
import numpy as np
import SimpleITK as sitk
import torch.utils.data as Data


class MyData(Data.Dataset):
    def __init__(self, files):

        self.files = files

    def __len__(self):

        return len(self.files)

    def load_nii(self, path):
        nii = sitk.ReadImage(path)
        nii = sitk.GetArrayFromImage(nii)
        return nii

    def save_nii(self, img, path):
        img = sitk.GetImageFromArray(img)
        sitk.WriteImage(img, path)

    def __getitem__(self, index):

        img_arr = self.load_nii(self.files[index])
        img_arr = img_arr.astype(np.float32)

        x = img_arr.shape[0]
        y = img_arr.shape[1]
        z = img_arr.shape[2]

        if x % 16 != 0:
            img = np.zeros(shape=(x + (16 - (x % 16)), y, z))
        elif (z % 32 != 0) | (y % 32 != 0):
            img = np.zeros(shape=(x, y + (32 - (y % 32)), z + (32 - (z % 32))))
        else:
            img = np.zeros(shape=(x, y, z))

        img[0:x, 0:y, 0:z] = img_arr

        img = img.astype(np.float32)
        img = img[np.newaxis, :, :, :]
        img = torch.from_numpy(img)
        img = img.type(torch.FloatTensor)

        # 只返回填充后的tensor
        return img
