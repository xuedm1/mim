import torch
import random
import numpy as np
import SimpleITK as sitk
import torch.utils.data as Data
from skimage.transform import resize


class MyData(Data.Dataset):
    def __init__(self, files, seed, new_h, new_w, new_z):
        self.files = files
        self.seed = seed
        self.new_h = new_h
        self.new_w = new_w
        self.new_z = new_z

    def __len__(self):
        return len(self.files)

    def load_nii(self, path):
        nii = sitk.ReadImage(path)
        nii = sitk.GetArrayFromImage(nii)
        return nii

    def save_nii(self, img, path):
        img = sitk.GetImageFromArray(img)
        sitk.WriteImage(img, path)

    def __getitem__(self, index):

        img_arr = self.load_nii(self.files[index])
        img_arr = img_arr.astype(np.float32)

        # random flip

        a = np.random.randint(0, 10)
        if a >= 6:
            np.flip(img_arr, axis=1)
        b = np.random.randint(0, 10)
        if b >= 6:
            np.flip(img_arr, axis=2)
        c = np.random.randint(0, 10)
        if c >= 6:
            np.flip(img_arr, axis=0)

        # random crop
        z = img_arr.shape[0]
        h = img_arr.shape[1]
        w = img_arr.shape[2]

        if self.seed is not None:
            random.seed(self.seed)
            top = np.random.randint(0, h - self.new_h)
        if self.seed is not None:
            random.seed(self.seed)
            left = np.random.randint(0, w - self.new_w)
        if self.seed is not None and z > 32:
            random.seed(self.seed)
            depth = np.random.randint(0, z - self.new_z)
        else:
            depth = 0

        img_arr = img_arr[depth : depth + self.new_z, top : top + self.new_h, left : left + self.new_w]

        img_arr = img_arr[np.newaxis, :, :, :]
        img_arr = torch.from_numpy(img_arr)
        img_arr = img_arr.type(torch.FloatTensor)

        return img_arr
