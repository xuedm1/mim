import torch
import numpy as np
import SimpleITK as sitk
import torch.utils.data as Data

class MyData(Data.Dataset):
    def __init__(self, files):
        self.files = files

    def __len__(self):
        return len(self.files)

    def load_nii(self, path):
        nii = sitk.ReadImage(path)
        nii = sitk.GetArrayFromImage(nii)
        return nii

    def __getitem__(self, index):
        # 直接加载图像数据
        img_arr = self.load_nii(self.files[index])
        img_arr = img_arr.astype(np.float32)

        # 添加一个新的维度来表示通道，PyTorch 期望的输入是 [C, D, H, W]
        img_arr = img_arr[np.newaxis, :, :, :]

        # 将 NumPy 数组转换为 PyTorch 张量
        img_tensor = torch.from_numpy(img_arr)
        img_tensor = img_tensor.type(torch.FloatTensor)

        # 直接返回图像张量，没有任何填充操作
        return img_tensor