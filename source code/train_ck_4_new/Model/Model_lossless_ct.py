import torch
import sys
import numpy as np
import Model.PixelCNN_light_ct as PixelCNN
import Model.learn_wavelet_trans_additive as learn_wavelet_trans_additive
import Model.learn_wavelet_trans_affine as learn_wavelet_trans_affine
import Model.learn_wavelet_trans_lossless_ct as learn_wavelet_trans_lossless
sys.path.append("..")


def anchor1_sequeeze(y):

    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H//2, W//2]).to(y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 0::2, 0::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 1::2, 1::2]

    return anchor

def anchor2_sequeeze(y):

    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H//2, W//2]).to(y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 1::2, 1::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 0::2, 0::2]

    return anchor

def anchor3_sequeeze(y):

    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H//2, W//2]).to(y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 0::2, 1::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 1::2, 0::2]

    return anchor

def anchor4_sequeeze(y):

    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H//2, W//2]).to(y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 1::2, 0::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 0::2, 1::2]

    return anchor


def ckbd_anchor_unsequeeze(anchor):
    B, C, Z, H, W = anchor.shape
    y_anchor = torch.zeros([B, C, Z, H, W * 2]).to(anchor.device)
    y_anchor[:, :, 0::2, 1::2, 1::2] = anchor[:, :, 0::2, 1::2, :]
    y_anchor[:, :, 0::2, 0::2, 0::2] = anchor[:, :, 0::2, 0::2, :]
    y_anchor[:, :, 1::2, 1::2, 0::2] = anchor[:, :, 1::2, 1::2, :]
    y_anchor[:, :, 1::2, 0::2, 1::2] = anchor[:, :, 1::2, 0::2, :]

    return y_anchor


def ckbd_nonanchor_unsequeeze(nonanchor):
    B, C, H, W = nonanchor.shape
    y_nonanchor = torch.zeros([B, C, H, W * 2]).to(nonanchor.device)
    y_nonanchor[:, :, 0::2, 1::2, 0::2] = nonanchor[:, :, 0::2, 1::2, :]
    y_nonanchor[:, :, 0::2, 0::2, 1::2] = nonanchor[:, :, 0::2, 0::2, :]
    y_nonanchor[:, :, 1::2, 1::2, 1::2] = nonanchor[:, :, 1::2, 1::2, :]
    y_nonanchor[:, :, 1::2, 0::2, 0::2] = nonanchor[:, :, 1::2, 0::2, :]

    return y_nonanchor


def anchor_mask(y):
    y[:, :, 0::2, 1::2, 0::2] = 1
    y[:, :, 0::2, 0::2, 1::2] = 2
    y[:, :, 1::2, 1::2, 1::2] = 3
    y[:, :, 1::2, 0::2, 0::2] = 4

    return y


def Get_high_bits(x):
    out = []
    for i in x:
        MSB = (i >> 8) & 0xFF # 高8位
        out.append(MSB)
    return out


def Get_low_bits(x):
    out = []
    for i in x:
        LSB = i & 0xFF # 低8位
        out.append(LSB)
    return out


def Get_sign(ori):
    out1 = []
    for ori_each in ori:
        sign = torch.ones_like(ori_each)
        sign[ori_each<0] = -1
        out1.append(sign)
    return out1


def Get_abs(x):
    out = []
    for i in x:
        i = torch.abs(i)
        out.append(i)
    return out
            

def Recon(x_l, x_h, x_s):

    reconstructed_data = ((x_h << 8) | x_l) * x_s
    
    return reconstructed_data


def concat1(x):
    out = torch.cat((anchor1_sequeeze(x), anchor2_sequeeze(x)), 1)
    return out

def concat2(x):
    out = torch.cat((anchor1_sequeeze(x), anchor2_sequeeze(x), anchor3_sequeeze(x)), 1)
    return out

def concat3(x):
    out = torch.cat((anchor1_sequeeze(x), anchor2_sequeeze(x), anchor3_sequeeze(x), anchor4_sequeeze(x)), 1)
    return out

def concat_(x1, x2):
    out = torch.cat((x1, x2), 1)
    return out

def concat1_(x1, x2, x3):
    out = torch.cat((x1, x2, x3), 1)
    return out


def cal_num(ori):
    num = 0
    for x in range(ori.size(2)):
                for y in range(ori.size(3)):
                    for z in range(ori.size(4)):
                        if ori[0,0,x,y,z] < 0:
                            num += 1
    return num


class Model(torch.nn.Module):
    def __init__(self, wavelet_affine):
        super(Model, self).__init__()

        self.trans_steps = 4
        self.wavelet_affine = wavelet_affine
        self.wavelet_transform_53 = learn_wavelet_trans_lossless.Wavelet()
        if self.wavelet_affine:
            self.wavelet_transform = torch.nn.ModuleList(
                learn_wavelet_trans_affine.Wavelet(True) for _i in range(self.trans_steps))
        else:
            self.wavelet_transform = learn_wavelet_trans_additive.Wavelet(True)

        self.coding_LLL_1 = PixelCNN.PixelCNN(2)
        self.coding_LLL_2 = PixelCNN.PixelCNN(3)
        self.coding_LLL_3 = PixelCNN.PixelCNN(4)
        self.coding_LLL_4 = PixelCNN.PixelCNN(5)

        self.coding_HLL_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(6) for _i in range(self.trans_steps)])
        self.coding_HLL_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(7) for _i in range(self.trans_steps)])
        self.coding_HLL_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(8) for _i in range(self.trans_steps)])
        self.coding_HLL_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(9) for _i in range(self.trans_steps)])

        self.coding_LHL_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(10) for _i in range(self.trans_steps)])
        self.coding_LHL_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(11) for _i in range(self.trans_steps)])
        self.coding_LHL_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(12) for _i in range(self.trans_steps)])
        self.coding_LHL_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(13) for _i in range(self.trans_steps)])

        self.coding_HHL_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(14) for _i in range(self.trans_steps)])
        self.coding_HHL_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(15) for _i in range(self.trans_steps)])
        self.coding_HHL_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(16) for _i in range(self.trans_steps)])
        self.coding_HHL_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(17) for _i in range(self.trans_steps)])

        self.coding_LLH_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(18) for _i in range(self.trans_steps)])
        self.coding_LLH_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(19) for _i in range(self.trans_steps)])
        self.coding_LLH_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(20) for _i in range(self.trans_steps)])
        self.coding_LLH_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(21) for _i in range(self.trans_steps)])

        self.coding_HLH_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(22) for _i in range(self.trans_steps)])
        self.coding_HLH_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(23) for _i in range(self.trans_steps)])
        self.coding_HLH_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(24) for _i in range(self.trans_steps)])
        self.coding_HLH_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(25) for _i in range(self.trans_steps)])

        self.coding_LHH_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(26) for _i in range(self.trans_steps)])
        self.coding_LHH_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(27) for _i in range(self.trans_steps)])
        self.coding_LHH_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(28) for _i in range(self.trans_steps)])
        self.coding_LHH_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(29) for _i in range(self.trans_steps)])

        self.coding_HHH_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(30) for _i in range(self.trans_steps)])
        self.coding_HHH_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(31) for _i in range(self.trans_steps)])
        self.coding_HHH_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(32) for _i in range(self.trans_steps)])
        self.coding_HHH_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(33) for _i in range(self.trans_steps)])

        self.mse_loss = torch.nn.MSELoss()

    def forward(self, x, train=1, wavelet_trainable=0, coding=1):

        # forward transform

        LLL = x

        HLL_list = []
        LHL_list = []
        HHL_list = []
        LLH_list = []
        HLH_list = []
        LHH_list = []
        HHH_list = []

        LLL_list_h = []
        HLL_list_h = []
        LHL_list_h = []
        HHL_list_h = []
        LLH_list_h = []
        HLH_list_h = []
        LHH_list_h = []
        HHH_list_h = []

        LLL_list_s = []
        HLL_list_s = []
        LHL_list_s = []
        HHL_list_s = []
        LLH_list_s = []
        HLH_list_s = []
        LHH_list_s = []
        HHH_list_s = []

        for i in range(self.trans_steps):

            if wavelet_trainable:
                if self.wavelet_affine:
                    LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH = self.wavelet_transform[i].forward_trans(LLL)
                else:
                    LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH = self.wavelet_transform.forward_trans(LLL)
            else:
                LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH = self.wavelet_transform_53.forward_trans(LLL)

            [LLL_s, HLL_s, LHL_s, HHL_s, LLH_s, HLH_s, LHH_s, HHH_s] = Get_sign([LLL.type(torch.int32), HLL.type(torch.int32), LHL.type(torch.int32), HHL.type(torch.int32), LLH.type(torch.int32), HLH.type(torch.int32), LHH.type(torch.int32), HHH.type(torch.int32)])
            [LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH] = Get_abs([LLL.type(torch.int32), HLL.type(torch.int32), LHL.type(torch.int32), HHL.type(torch.int32), LLH.type(torch.int32), HLH.type(torch.int32), LHH.type(torch.int32), HHH.type(torch.int32)])

            [LLL_h, HLL_h, LHL_h, HHL_h, LLH_h, HLH_h, LHH_h, HHH_h] = Get_high_bits([LLL.type(torch.int32), HLL.type(torch.int32), LHL.type(torch.int32), HHL.type(torch.int32), LLH.type(torch.int32), HLH.type(torch.int32), LHH.type(torch.int32), HHH.type(torch.int32)])
            [LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH] = Get_low_bits([LLL.type(torch.int32), HLL.type(torch.int32), LHL.type(torch.int32), HHL.type(torch.int32), LLH.type(torch.int32), HLH.type(torch.int32), LHH.type(torch.int32), HHH.type(torch.int32)])
            
            HLL_list.append(HLL)
            LHL_list.append(LHL)
            HHL_list.append(HHL)
            LLH_list.append(LLH)
            HLH_list.append(HLH)
            LHH_list.append(LHH)
            HHH_list.append(HHH)

            LLL_list_h.append(LLL_h)
            HLL_list_h.append(HLL_h)
            LHL_list_h.append(LHL_h)
            HHL_list_h.append(HHL_h)
            LLH_list_h.append(LLH_h)
            HLH_list_h.append(HLH_h)
            LHH_list_h.append(LHH_h)
            HHH_list_h.append(HHH_h)

            LLL_list_s.append(LLL_s)
            HLL_list_s.append(HLL_s)
            LHL_list_s.append(LHL_s)
            HHL_list_s.append(HHL_s)
            LLH_list_s.append(LLH_s)
            HLH_list_s.append(HLH_s)
            LHH_list_s.append(LHH_s)
            HHH_list_s.append(HHH_s)

        # print(LLL.shape)

        bits = self.coding_LLL_1(anchor1_sequeeze(LLL), concat_(anchor1_sequeeze(LLL_h), anchor1_sequeeze(LLL_s)))
        bits = bits + self.coding_LLL_2(anchor2_sequeeze(LLL), concat1_(anchor1_sequeeze(LLL), anchor2_sequeeze(LLL_h), anchor2_sequeeze(LLL_s)))
        bits = bits + self.coding_LLL_3(anchor3_sequeeze(LLL), concat1_(concat1(LLL), anchor3_sequeeze(LLL_h), anchor3_sequeeze(LLL_s)))
        bits = bits + self.coding_LLL_4(anchor4_sequeeze(LLL), concat1_(concat2(LLL), anchor4_sequeeze(LLL_h), anchor4_sequeeze(LLL_s)))


        for i in range(self.trans_steps):

            j = self.trans_steps - 1 - i

            bits = bits + self.coding_HLL_list_1[j](anchor1_sequeeze(HLL_list[j]), concat1_(concat3(LLL), anchor1_sequeeze(HLL_list_h[j]), anchor1_sequeeze(HLL_list_s[j])))
            bits = bits + self.coding_HLL_list_2[j](anchor2_sequeeze(HLL_list[j]),
                                                    torch.cat((concat3(LLL), anchor1_sequeeze(HLL_list[j]), anchor2_sequeeze(HLL_list_h[j]), anchor2_sequeeze(HLL_list_s[j])), 1))
            bits = bits + self.coding_HLL_list_3[j](anchor3_sequeeze(HLL_list[j]),
                                                    torch.cat((concat3(LLL), concat1(HLL_list[j]), anchor3_sequeeze(HLL_list_h[j]), anchor3_sequeeze(HLL_list_s[j])), 1))
            bits = bits + self.coding_HLL_list_4[j](anchor4_sequeeze(HLL_list[j]),
                                                    torch.cat((concat3(LLL), concat2(HLL_list[j]), anchor4_sequeeze(HLL_list_h[j]), anchor4_sequeeze(HLL_list_s[j])), 1))

            bits = bits + self.coding_LHL_list_1[j](anchor1_sequeeze(LHL_list[j]),
                                                    torch.cat((concat3(LLL), concat3(HLL_list[j]), anchor1_sequeeze(LHL_list_h[j]), anchor1_sequeeze(LHL_list_s[j])), 1))
            bits = bits + self.coding_LHL_list_2[j](anchor2_sequeeze(LHL_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), anchor1_sequeeze(LHL_list[j]), anchor2_sequeeze(LHL_list_h[j]), anchor2_sequeeze(LHL_list_s[j])), 1))
            bits = bits + self.coding_LHL_list_3[j](anchor3_sequeeze(LHL_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat1(LHL_list[j]), anchor3_sequeeze(LHL_list_h[j]), anchor3_sequeeze(LHL_list_s[j])), 1))
            bits = bits + self.coding_LHL_list_4[j](anchor4_sequeeze(LHL_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat2(LHL_list[j]), anchor4_sequeeze(LHL_list_h[j]), anchor4_sequeeze(LHL_list_s[j])), 1))


            bits = bits + self.coding_HHL_list_1[j](anchor1_sequeeze(HHL_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), anchor1_sequeeze(HHL_list_h[j]), anchor1_sequeeze(HHL_list_s[j])), 1))
            bits = bits + self.coding_HHL_list_2[j](anchor2_sequeeze(HHL_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), anchor1_sequeeze(HHL_list[j]), anchor2_sequeeze(HHL_list_h[j]), anchor2_sequeeze(HHL_list_s[j])), 1))
            bits = bits + self.coding_HHL_list_3[j](anchor3_sequeeze(HHL_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat1(HHL_list[j]), anchor3_sequeeze(HHL_list_h[j]), anchor3_sequeeze(HHL_list_s[j])), 1))
            bits = bits + self.coding_HHL_list_4[j](anchor4_sequeeze(HHL_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat2(HHL_list[j]), anchor4_sequeeze(HHL_list_h[j]), anchor4_sequeeze(HHL_list_s[j])), 1))


            bits = bits + self.coding_LLH_list_1[j](anchor1_sequeeze(LLH_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]), anchor1_sequeeze(LLH_list_h[j]), anchor1_sequeeze(LLH_list_s[j])), 1))
            bits = bits + self.coding_LLH_list_2[j](anchor2_sequeeze(LLH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              anchor1_sequeeze(
                                                                                                  LLH_list[j]), anchor2_sequeeze(LLH_list_h[j]), anchor2_sequeeze(LLH_list_s[j])), 1))
            bits = bits + self.coding_LLH_list_3[j](anchor3_sequeeze(LLH_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]), concat1(LLH_list[j]), anchor3_sequeeze(LLH_list_h[j]), anchor3_sequeeze(LLH_list_s[j])),
                1))
            bits = bits + self.coding_LLH_list_4[j](anchor4_sequeeze(LLH_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]), concat2(LLH_list[j]), anchor4_sequeeze(LLH_list_h[j]), anchor4_sequeeze(LLH_list_s[j])),
                1))


            bits = bits + self.coding_HLH_list_1[j](anchor1_sequeeze(HLH_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]), concat3(LLH_list[j]), anchor1_sequeeze(HLH_list_h[j]), anchor1_sequeeze(HLH_list_s[j])),
                1))
            bits = bits + self.coding_HLH_list_2[j](anchor2_sequeeze(HLH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              anchor1_sequeeze(
                                                                                                  HLH_list[j]), anchor2_sequeeze(HLH_list_h[j]), anchor2_sequeeze(HLH_list_s[j])), 1))
            bits = bits + self.coding_HLH_list_3[j](anchor3_sequeeze(HLH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat1(HLH_list[j]), anchor3_sequeeze(HLH_list_h[j]), anchor3_sequeeze(HLH_list_s[j])), 1))
            bits = bits + self.coding_HLH_list_4[j](anchor4_sequeeze(HLH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat2(HLH_list[j]), anchor4_sequeeze(HLH_list_h[j]), anchor4_sequeeze(HLH_list_s[j])), 1))


            bits = bits + self.coding_LHH_list_1[j](anchor1_sequeeze(LHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]), anchor1_sequeeze(LHH_list_h[j]), anchor1_sequeeze(LHH_list_s[j])), 1))
            bits = bits + self.coding_LHH_list_2[j](anchor2_sequeeze(LHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),
                                                                                              anchor1_sequeeze(
                                                                                                  LHH_list[j]), anchor2_sequeeze(LHH_list_h[j]), anchor2_sequeeze(LHH_list_s[j])), 1))
            bits = bits + self.coding_LHH_list_3[j](anchor3_sequeeze(LHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),
                                                                                              concat1(LHH_list[j]), anchor3_sequeeze(LHH_list_h[j]), anchor3_sequeeze(LHH_list_s[j])), 1))
            bits = bits + self.coding_LHH_list_4[j](anchor4_sequeeze(LHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),
                                                                                              concat2(LHH_list[j]), anchor4_sequeeze(LHH_list_h[j]), anchor4_sequeeze(LHH_list_s[j])), 1))


            bits = bits + self.coding_HHH_list_1[j](anchor1_sequeeze(HHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),
                                                                                              concat3(LHH_list[j]), anchor1_sequeeze(HHH_list_h[j]), anchor1_sequeeze(HHH_list_s[j])), 1))
            bits = bits + self.coding_HHH_list_2[j](anchor2_sequeeze(HHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),
                                                                                              concat3(LHH_list[j]),
                                                                                              anchor1_sequeeze(
                                                                                                  HHH_list[j]), anchor2_sequeeze(HHH_list_h[j]), anchor2_sequeeze(HHH_list_s[j])), 1))
            bits = bits + self.coding_HHH_list_3[j](anchor3_sequeeze(HHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),
                                                                                              concat3(LHH_list[j]),
                                                                                              concat1(HHH_list[j]), anchor3_sequeeze(HHH_list_h[j]), anchor3_sequeeze(HHH_list_s[j])), 1))
            bits = bits + self.coding_HHH_list_4[j](anchor4_sequeeze(HHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),
                                                                                              concat3(LHH_list[j]),
                                                                                              concat2(HHH_list[j]), anchor4_sequeeze(HHH_list_h[j]), anchor4_sequeeze(HHH_list_s[j])), 1))

            
            HLL_list[j] = Recon(HLL_list[j], HLL_list_h[j], HLL_list_s[j])
            LHL_list[j] = Recon(LHL_list[j], LHL_list_h[j], LHL_list_s[j])
            HHL_list[j] = Recon(HHL_list[j], HHL_list_h[j], HHL_list_s[j])
            LLH_list[j] = Recon(LLH_list[j], LLH_list_h[j], LLH_list_s[j])
            HLH_list[j] = Recon(HLH_list[j], HLH_list_h[j], HLH_list_s[j])
            LHH_list[j] = Recon(LHH_list[j], LHH_list_h[j], LHH_list_s[j])
            HHH_list[j] = Recon(HHH_list[j], HHH_list_h[j], HHH_list_s[j])
            
            LLL = Recon(LLL, LLL_list_h[j], LLL_list_s[j])

            
            if wavelet_trainable:
                if self.wavelet_affine:
                    LLL = self.wavelet_transform[j].inverse_trans(LLL, HLL_list[j], LHL_list[j], HHL_list[j],
                                                                  LLH_list[j], HLH_list[j], LHH_list[j], HHH_list[j])
                else:
                    LLL = self.wavelet_transform.inverse_trans(LLL, HLL_list[j], LHL_list[j], HHL_list[j], LLH_list[j],
                                                               HLH_list[j], LHH_list[j], HHH_list[j])
            else:
                LLL = self.wavelet_transform_53.inverse_trans(LLL, HLL_list[j], LHL_list[j], HHL_list[j], LLH_list[j],
                                                              HLH_list[j], LHH_list[j], HHH_list[j])
            
            LLL = LLL.type(torch.int32)
        
        return self.mse_loss(x, LLL), bits, LLL


