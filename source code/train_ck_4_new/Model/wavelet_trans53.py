import torch
from torch.nn import functional as F


class CDF53(torch.nn.Module):
    def __init__(self):
        super(CDF53, self).__init__()
        # Filter coefficients of bior2.2 wavelet (Orthogonal form of CDF 5/3 wavelet）
        self.lifting_coeff = [-0.5, 0.25]

    def forward_trans(self, L, H):
        # L[1:end+1] + L[0:end]
        paddings = (0, 0, 0, 0, 0, 1)
        tmp = F.pad(L, paddings, "circular")
        tmp = tmp[:, :, 1::, :, :]

        H = H + torch.round(self.lifting_coeff[0] * (L + tmp))

        paddings = (0, 0, 0, 0, 0, 1)
        tmp = F.pad(H, paddings, 'circular')
        tmp = tmp[:, :, 0:-1, :, :]
        # print(H.shape, tmp.shape)
        L = L + torch.round(self.lifting_coeff[1] * (H + tmp))

        return L, H

    def inverse_trans(self, L, H):
        paddings = (0, 0, 0, 0, 0, 1)
        tmp = F.pad(H, paddings, 'circular')
        tmp = tmp[:, :, 0:-1, :, :]
        L = L - torch.round(self.lifting_coeff[1] * (H + tmp))

        paddings = (0, 0, 0, 0, 0, 1)
        tmp = F.pad(L, paddings, "circular")
        tmp = tmp[:, :, 1::, :, :]
        H = H - torch.round(self.lifting_coeff[0] * (L + tmp))

        return L, H


class Wavelet(torch.nn.Module):
    def __init__(self):
        super(Wavelet, self).__init__()

        self.lifting = CDF53()

    def forward_trans(self, x):
        # transform for rows

        # 1
        L = x[:, :, 0::2, :, :]
        H = x[:, :, 1::2, :, :]
        L, H = self.lifting.forward_trans(L, H)

        # 2
        L = L.permute(0, 1, 3, 2, 4)
        LL = L[:, :, 0::2, :, :]
        HL = L[:, :, 1::2, :, :]
        LL, HL = self.lifting.forward_trans(LL, HL)
        LL = LL.permute(0, 1, 3, 2, 4)
        HL = HL.permute(0, 1, 3, 2, 4)

        H = H.permute(0, 1, 3, 2, 4)
        LH = H[:, :, 0::2, :, :]
        HH = H[:, :, 1::2, :, :]
        LH, HH = self.lifting.forward_trans(LH, HH)
        LH = LH.permute(0, 1, 3, 2, 4)
        HH = HH.permute(0, 1, 3, 2, 4)

        # 3
        LL = LL.permute(0, 1, 4, 3, 2)
        LLL = LL[:, :, 0::2, :, :]
        HLL = LL[:, :, 1::2, :, :]
        LLL, HLL = self.lifting.forward_trans(LLL, HLL)
        LLL = LLL.permute(0, 1, 4, 3, 2)
        HLL = HLL.permute(0, 1, 4, 3, 2)

        HL = HL.permute(0, 1, 4, 3, 2)
        LHL = HL[:, :, 0::2, :, :]
        HHL = HL[:, :, 1::2, :, :]
        LHL, HHL = self.lifting.forward_trans(LHL, HHL)
        LHL = LHL.permute(0, 1, 4, 3, 2)
        HHL = HHL.permute(0, 1, 4, 3, 2)

        LH = LH.permute(0, 1, 4, 3, 2)
        LLH = LH[:, :, 0::2, :, :]
        HLH = LH[:, :, 1::2, :, :]
        LLH, HLH = self.lifting.forward_trans(LLH, HLH)
        LLH = LLH.permute(0, 1, 4, 3, 2)
        HLH = HLH.permute(0, 1, 4, 3, 2)

        HH = HH.permute(0, 1, 4, 3, 2)
        LHH = HH[:, :, 0::2, :, :]
        HHH = HH[:, :, 1::2, :, :]
        LHH, HHH = self.lifting.forward_trans(LHH, HHH)
        LHH = LHH.permute(0, 1, 4, 3, 2)
        HHH = HHH.permute(0, 1, 4, 3, 2)

        return LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH

    def inverse_trans(self, LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH):
        # 3
        LLL = LLL.permute(0, 1, 4, 3, 2)
        HLL = HLL.permute(0, 1, 4, 3, 2)
        LL = torch.zeros(LLL.size()[0], LLL.size()[1], LLL.size()[2] + HLL.size()[2], LLL.size()[3], LLL.size()[4],
                         device="cuda")
        LLL, HLL = self.lifting.inverse_trans(LLL, HLL)
        LL[:, :, 0::2, :, :] = LLL
        LL[:, :, 1::2, :, :] = HLL
        LL = LL.permute(0, 1, 4, 3, 2)

        LHL = LHL.permute(0, 1, 4, 3, 2)
        HHL = HHL.permute(0, 1, 4, 3, 2)
        HL = torch.zeros(LHL.size()[0], LHL.size()[1], LHL.size()[2] + HHL.size()[2], LHL.size()[3], LHL.size()[4],
                         device="cuda")
        LHL, HHL = self.lifting.inverse_trans(LHL, HHL)
        HL[:, :, 0::2, :, :] = LHL
        HL[:, :, 1::2, :, :] = HHL
        HL = HL.permute(0, 1, 4, 3, 2)

        LLH = LLH.permute(0, 1, 4, 3, 2)
        HLH = HLH.permute(0, 1, 4, 3, 2)
        LH = torch.zeros(LLH.size()[0], LLH.size()[1], LLH.size()[2] + HLH.size()[2], LLH.size()[3], LLH.size()[4],
                         device="cuda")
        LLH, HLH = self.lifting.inverse_trans(LLH, HLH)
        LH[:, :, 0::2, :, :] = LLH
        LH[:, :, 1::2, :, :] = HLH
        LH = LH.permute(0, 1, 4, 3, 2)

        LHH = LHH.permute(0, 1, 4, 3, 2)
        HHH = HHH.permute(0, 1, 4, 3, 2)
        HH = torch.zeros(LHH.size()[0], LHH.size()[1], LHH.size()[2] + HHH.size()[2], LHH.size()[3], LHH.size()[4],
                         device="cuda")
        LHH, HHH = self.lifting.inverse_trans(LHH, HHH)
        HH[:, :, 0::2, :, :] = LHH
        HH[:, :, 1::2, :, :] = HHH
        HH = HH.permute(0, 1, 4, 3, 2)

        # 2
        LH = LH.permute(0, 1, 3, 2, 4)
        HH = HH.permute(0, 1, 3, 2, 4)
        H = torch.zeros(LH.size()[0], LH.size()[1], LH.size()[2] + HH.size()[2], LH.size()[3], LH.size()[4],
                        device="cuda")
        LH, HH = self.lifting.inverse_trans(LH, HH)
        H[:, :, 0::2, :, :] = LH
        H[:, :, 1::2, :, :] = HH
        H = H.permute(0, 1, 3, 2, 4)

        LL = LL.permute(0, 1, 3, 2, 4)
        HL = HL.permute(0, 1, 3, 2, 4)
        L = torch.zeros(LL.size()[0], LL.size()[1], LL.size()[2] + HL.size()[2], LL.size()[3], LL.size()[4],
                        device="cuda")
        LL, HL = self.lifting.inverse_trans(LL, HL)
        L[:, :, 0::2, :, :] = LL
        L[:, :, 1::2, :, :] = HL
        L = L.permute(0, 1, 3, 2, 4)

        # 1
        L, H = self.lifting.inverse_trans(L, H)
        x = torch.zeros(L.size()[0], L.size()[1], L.size()[2] + H.size()[2], L.size()[3], L.size()[4], device="cuda")
        x[:, :, 0::2, :, :] = L
        x[:, :, 1::2, :, :] = H

        return x
