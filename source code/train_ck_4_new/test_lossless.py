from Util.MyData_test_new import MyData
from torch.utils.data import DataLoader
import SimpleITK as sitk
from Model.Model_lossless_sign_high5 import Model
import argparse
import torch
from torch.autograd import Variable
import os
import numpy as np
import glob

parser = argparse.ArgumentParser(description='.')

# parameters
parser.add_argument('--input_dir', type=str, default='/data/xiaoban/cfe/test_data/MRI')
parser.add_argument('--log_dir', type=str, default='/data/xiaoban/cfe/lossy_result/ct_ck_new_lossless')
parser.add_argument('--load_model', type=str, default='/data/xiaoban/cfe/checkpoint/mri_sign5/model_epoch01760.pth')
parser.add_argument('--output_path', type=str, default='/data/xiaoban/cfe/lossy_result/ct_ck_new_lossless_recon')
parser.add_argument('--epochs', type=int, default=300000)
parser.add_argument('--batch_size', type=int, default=1)
parser.add_argument('--main_model', type=str, default=None)


def to_variable(x):
    if torch.cuda.is_available():
        x = x.cuda()
    return Variable(x)


def save_nii(img, path):
    img = sitk.GetImageFromArray(img)
    sitk.WriteImage(img, path)


def main():
    args = parser.parse_args()

    if not os.path.exists(args.log_dir):
        os.makedirs(args.log_dir)
    logfile = open(args.log_dir + '/ct_ck_new_lossless.txt', 'w')
    if not os.path.exists(args.output_path):
        os.makedirs(args.output_path)

    if args.load_model is not None:
        checkpoint = torch.load(args.load_model)
        model = Model(wavelet_affine = False)
        state_dict = checkpoint['state_dict']
        state_dict_new = {}
        print(checkpoint.keys())
        for k,v in state_dict.items():
            new_k = k.replace('module.', '') if 'module.' in k else keys
            state_dict_new[new_k] = v

        model.load_state_dict(state_dict_new)
        # model.load_state_dict(state_dict)
        print('Load pre-trained model [' + args.load_model + '] succeed!')
        logfile.write('Load pre-trained model [' + args.load_model + '] succeed!' + '\n')
        logfile.flush()

    if torch.cuda.is_available():
        if torch.cuda.device_count() > 1:
            print("Let's use", torch.cuda.device_count(), "GPUs!")
            model = torch.nn.DataParallel(model).cuda()
        else:
            model = model.cuda()

    logfile.write('Load data starting...' + '\n')
    logfile.flush()

    logfile.write('Load all data succeed!' + '\n')
    logfile.flush()

    test_files = glob.glob(os.path.join(args.input_dir, '*.nii'))
    test_files.sort()
    DS = MyData(files=test_files)
    print("Number of testing images: ", len(DS))
    test_loader = DataLoader(dataset=DS, batch_size=args.batch_size, shuffle=False, drop_last=False)
    max_step = test_loader.__len__()

    psnr_all = 0.
    bpp_all = 0.

    for batch_idx, input_img in enumerate(test_loader):
        input_img_v = to_variable(input_img[1])
        input_label_v = to_variable(input_img[0])

        with torch.no_grad():
            mse1, bpp, recon = model(input_img_v, train=0, wavelet_trainable=0)

        psnr = (10. * torch.log10(65535. * 65535. / mse1)).item()

        recon = recon[0, 0, 0:input_label_v.shape[2], :, :]
        recon = recon.cpu().data.numpy().astype(np.float32)
        save_nii(recon, args.output_path + '/' + str(batch_idx+1).zfill(3) + '.nii')

        bpp = torch.sum(bpp)
        size = input_label_v.size()
        num_pixels = size[0] * size[2] * size[3] * size[4]
        bpp = bpp / num_pixels

        psnr_all = psnr_all + psnr
        bpp_all = bpp_all + bpp.item()

        print('image ID:', batch_idx)
        print('psnr', psnr)
        print('bpp', bpp.item())

        logfile.write('psnr:' + str(psnr))
        logfile.write('bpp:' + str(bpp.item()))
        logfile.flush()
    print('psnr_mean: ' + str(psnr_all / max_step))
    print('bpp_mean: ' + str(bpp_all / max_step))

    logfile.write('psnr_mean:' + str(psnr_all))
    logfile.write('bpp_mean:' + str(bpp_all))
    logfile.flush()
    logfile.close()


if __name__ == "__main__":
    main()
