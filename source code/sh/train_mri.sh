pip install einops xlsxwriter openpyxl imagesize pybind11 SimpleITK tensorboardX scikit-image matplotlib scipy -i https://pypi.mirrors.ustc.edu.cn/simple/
cd /code/aiWave_rans/inference_rans/rans_better
python setup.py build
python setup.py install
cd /code/aiWave_rans/train_ck_4_new
python train_lossy_mri.py