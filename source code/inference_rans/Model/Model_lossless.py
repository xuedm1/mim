import torch
import sys
import numpy as np


sys.path.append("..")
import Model.PixelCNN_light5 as PixelCNN
import Model.learn_wavelet_trans_additive as learn_wavelet_trans_additive
import Model.learn_wavelet_trans_affine as learn_wavelet_trans_affine
import Model.learn_wavelet_trans_lossless as learn_wavelet_trans_lossless
import Model.unet as Unet


def anchor1_sequeeze(y):

    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H//2, W//2]).to(y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 0::2, 0::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 1::2, 1::2]

    # print(anchor)

    return anchor

def anchor2_sequeeze(y):

    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H//2, W//2]).to(y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 1::2, 1::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 0::2, 0::2]

    # print(anchor)

    return anchor

def anchor3_sequeeze(y):

    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H//2, W//2]).to(y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 0::2, 1::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 1::2, 0::2]

    # print(anchor)

    return anchor

def anchor4_sequeeze(y):

    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H//2, W//2]).to(y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 1::2, 0::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 0::2, 1::2]

    # print(anchor)

    return anchor


def ckbd_anchor_unsequeeze(anchor):
    B, C, Z, H, W = anchor.shape
    y_anchor = torch.zeros([B, C, Z, H, W * 2]).to(anchor.device)
    y_anchor[:, :, 0::2, 1::2, 1::2] = anchor[:, :, 0::2, 1::2, :]
    y_anchor[:, :, 0::2, 0::2, 0::2] = anchor[:, :, 0::2, 0::2, :]
    y_anchor[:, :, 1::2, 1::2, 0::2] = anchor[:, :, 1::2, 1::2, :]
    y_anchor[:, :, 1::2, 0::2, 1::2] = anchor[:, :, 1::2, 0::2, :]

    print(y_anchor)

    return y_anchor


def ckbd_nonanchor_unsequeeze(nonanchor):
    B, C, H, W = nonanchor.shape
    y_nonanchor = torch.zeros([B, C, H, W * 2]).to(nonanchor.device)
    y_nonanchor[:, :, 0::2, 1::2, 0::2] = nonanchor[:, :, 0::2, 1::2, :]
    y_nonanchor[:, :, 0::2, 0::2, 1::2] = nonanchor[:, :, 0::2, 0::2, :]
    y_nonanchor[:, :, 1::2, 1::2, 1::2] = nonanchor[:, :, 1::2, 1::2, :]
    y_nonanchor[:, :, 1::2, 0::2, 0::2] = nonanchor[:, :, 1::2, 0::2, :]

    print(y_nonanchor)

    return y_nonanchor


def anchor_mask(y):
    y[:, :, 0::2, 1::2, 0::2] = 1
    y[:, :, 0::2, 0::2, 1::2] = 2
    y[:, :, 1::2, 1::2, 1::2] = 3
    y[:, :, 1::2, 0::2, 0::2] = 4

    print(y)

    return y


def concat1(x):
    out = torch.cat((anchor1_sequeeze(x), anchor2_sequeeze(x)), 1)
    return out

def concat2(x):
    out = torch.cat((anchor1_sequeeze(x), anchor2_sequeeze(x), anchor3_sequeeze(x)), 1)
    return out

def concat3(x):
    out = torch.cat((anchor1_sequeeze(x), anchor2_sequeeze(x), anchor3_sequeeze(x), anchor4_sequeeze(x)), 1)
    return out

def concat_(x1, x2):
    out = torch.cat((x1, x2), 1)
    return out


class Model(torch.nn.Module):
    def __init__(self, wavelet_affine):
        super(Model, self).__init__()

        self.trans_steps = 4
        self.wavelet_affine = wavelet_affine
        self.wavelet_transform_53 = learn_wavelet_trans_lossless.Wavelet()
        if self.wavelet_affine:
            self.wavelet_transform = torch.nn.ModuleList(
                learn_wavelet_trans_affine.Wavelet(True) for _i in range(self.trans_steps))
        else:
            self.wavelet_transform = learn_wavelet_trans_additive.Wavelet(True)

        self.Highbit_Feature = Unet.UnetModel(1, 1)

        self.coding_LLL_1 = PixelCNN.PixelCNN(1)
        self.coding_LLL_2 = PixelCNN.PixelCNN(2)
        self.coding_LLL_3 = PixelCNN.PixelCNN(3)
        self.coding_LLL_4 = PixelCNN.PixelCNN(4)

        self.coding_HLL_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(5) for _i in range(self.trans_steps)])
        self.coding_HLL_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(6) for _i in range(self.trans_steps)])
        self.coding_HLL_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(7) for _i in range(self.trans_steps)])
        self.coding_HLL_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(8) for _i in range(self.trans_steps)])

        self.coding_LHL_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(9) for _i in range(self.trans_steps)])
        self.coding_LHL_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(10) for _i in range(self.trans_steps)])
        self.coding_LHL_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(11) for _i in range(self.trans_steps)])
        self.coding_LHL_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(12) for _i in range(self.trans_steps)])

        self.coding_HHL_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(13) for _i in range(self.trans_steps)])
        self.coding_HHL_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(14) for _i in range(self.trans_steps)])
        self.coding_HHL_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(15) for _i in range(self.trans_steps)])
        self.coding_HHL_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(16) for _i in range(self.trans_steps)])

        self.coding_LLH_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(17) for _i in range(self.trans_steps)])
        self.coding_LLH_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(18) for _i in range(self.trans_steps)])
        self.coding_LLH_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(19) for _i in range(self.trans_steps)])
        self.coding_LLH_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(20) for _i in range(self.trans_steps)])

        self.coding_HLH_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(21) for _i in range(self.trans_steps)])
        self.coding_HLH_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(22) for _i in range(self.trans_steps)])
        self.coding_HLH_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(23) for _i in range(self.trans_steps)])
        self.coding_HLH_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(24) for _i in range(self.trans_steps)])

        self.coding_LHH_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(25) for _i in range(self.trans_steps)])
        self.coding_LHH_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(26) for _i in range(self.trans_steps)])
        self.coding_LHH_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(27) for _i in range(self.trans_steps)])
        self.coding_LHH_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(28) for _i in range(self.trans_steps)])

        self.coding_HHH_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(29) for _i in range(self.trans_steps)])
        self.coding_HHH_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(30) for _i in range(self.trans_steps)])
        self.coding_HHH_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(31) for _i in range(self.trans_steps)])
        self.coding_HHH_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(32) for _i in range(self.trans_steps)])

        self.mse_loss = torch.nn.MSELoss()

    def forward(self, x, x_h, train, wavelet_trainable, coding=1):

        # forward transform

        LLL = x
        HLL_list = []
        LHL_list = []
        HHL_list = []
        LLH_list = []
        HLH_list = []
        LHH_list = []
        HHH_list = []

        for i in range(self.trans_steps):
            if wavelet_trainable:
                if self.wavelet_affine:
                    LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH = self.wavelet_transform[i].forward_trans(LLL)
                else:
                    LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH = self.wavelet_transform.forward_trans(LLL)
            else:
                LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH = self.wavelet_transform_53.forward_trans(LLL)

            HLL_list.append(HLL)
            LHL_list.append(LHL)
            HHL_list.append(HHL)
            LLH_list.append(LLH)
            HLH_list.append(HLH)
            LHH_list.append(LHH)
            HHH_list.append(HHH)

        feature_high = self.Highbit_Feature(x_h) 

        bits = self.coding_LLL_1(anchor1_sequeeze(LLL), feature_high[3])
        bits = bits + self.coding_LLL_2(anchor2_sequeeze(LLL), concat_(anchor1_sequeeze(LLL), feature_high[3]))
        bits = bits + self.coding_LLL_3(anchor3_sequeeze(LLL), concat_(concat1(LLL), feature_high[3]))
        bits = bits + self.coding_LLL_4(anchor4_sequeeze(LLL), concat_(concat2(LLL), feature_high[3]))

        for i in range(self.trans_steps):

            j = self.trans_steps - 1 - i

            bits = bits + self.coding_HLL_list_1[j](anchor1_sequeeze(HLL_list[j]), concat_(concat3(LLL), feature_high[j]))
            bits = bits + self.coding_HLL_list_2[j](anchor2_sequeeze(HLL_list[j]),
                                                    torch.cat((concat3(LLL), anchor1_sequeeze(HLL_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_HLL_list_3[j](anchor3_sequeeze(HLL_list[j]),
                                                    torch.cat((concat3(LLL), concat1(HLL_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_HLL_list_4[j](anchor4_sequeeze(HLL_list[j]),
                                                    torch.cat((concat3(LLL), concat2(HLL_list[j]), feature_high[j]), 1))

            bits = bits + self.coding_LHL_list_1[j](anchor1_sequeeze(LHL_list[j]),
                                                    torch.cat((concat3(LLL), concat3(HLL_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_LHL_list_2[j](anchor2_sequeeze(LHL_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), anchor1_sequeeze(LHL_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_LHL_list_3[j](anchor3_sequeeze(LHL_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat1(LHL_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_LHL_list_4[j](anchor4_sequeeze(LHL_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat2(LHL_list[j]), feature_high[j]), 1))


            bits = bits + self.coding_HHL_list_1[j](anchor1_sequeeze(HHL_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_HHL_list_2[j](anchor2_sequeeze(HHL_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), anchor1_sequeeze(HHL_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_HHL_list_3[j](anchor3_sequeeze(HHL_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat1(HHL_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_HHL_list_4[j](anchor4_sequeeze(HHL_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat2(HHL_list[j]), feature_high[j]), 1))


            bits = bits + self.coding_LLH_list_1[j](anchor1_sequeeze(LLH_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_LLH_list_2[j](anchor2_sequeeze(LLH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              anchor1_sequeeze(
                                                                                                  LLH_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_LLH_list_3[j](anchor3_sequeeze(LLH_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]), concat1(LLH_list[j]), feature_high[j]),
                1))
            bits = bits + self.coding_LLH_list_4[j](anchor4_sequeeze(LLH_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]), concat2(LLH_list[j]), feature_high[j]),
                1))


            bits = bits + self.coding_HLH_list_1[j](anchor1_sequeeze(HLH_list[j]), torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]), concat3(LLH_list[j]), feature_high[j]),
                1))
            bits = bits + self.coding_HLH_list_2[j](anchor2_sequeeze(HLH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              anchor1_sequeeze(
                                                                                                  HLH_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_HLH_list_3[j](anchor3_sequeeze(HLH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat1(HLH_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_HLH_list_4[j](anchor4_sequeeze(HLH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat2(HLH_list[j]), feature_high[j]), 1))


            bits = bits + self.coding_LHH_list_1[j](anchor1_sequeeze(LHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_LHH_list_2[j](anchor2_sequeeze(LHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),
                                                                                              anchor1_sequeeze(
                                                                                                  LHH_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_LHH_list_3[j](anchor3_sequeeze(LHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),
                                                                                              concat1(LHH_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_LHH_list_4[j](anchor4_sequeeze(LHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),
                                                                                              concat2(LHH_list[j]), feature_high[j]), 1))


            bits = bits + self.coding_HHH_list_1[j](anchor1_sequeeze(HHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),
                                                                                              concat3(LHH_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_HHH_list_2[j](anchor2_sequeeze(HHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),
                                                                                              concat3(LHH_list[j]),
                                                                                              anchor1_sequeeze(
                                                                                                  HHH_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_HHH_list_3[j](anchor3_sequeeze(HHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),
                                                                                              concat3(LHH_list[j]),
                                                                                              concat1(HHH_list[j]), feature_high[j]), 1))
            bits = bits + self.coding_HHH_list_4[j](anchor4_sequeeze(HHH_list[j]), torch.cat((concat3(LLL),
                                                                                              concat3(HLL_list[j]),
                                                                                              concat3(LHL_list[j]),
                                                                                              concat3(HHL_list[j]),
                                                                                              concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),
                                                                                              concat3(LHH_list[j]),
                                                                                              concat2(HHH_list[j]), feature_high[j]), 1))

            if wavelet_trainable:
                if self.wavelet_affine:
                    LLL = self.wavelet_transform[j].inverse_trans(LLL, HLL_list[j], LHL_list[j], HHL_list[j],
                                                                  LLH_list[j], HLH_list[j], LHH_list[j], HHH_list[j])
                else:
                    LLL = self.wavelet_transform.inverse_trans(LLL, HLL_list[j], LHL_list[j], HHL_list[j], LLH_list[j],
                                                               HLH_list[j], LHH_list[j], HHH_list[j])
            else:
                LLL = self.wavelet_transform_53.inverse_trans(LLL, HLL_list[j], LHL_list[j], HHL_list[j], LLH_list[j],
                                                              HLH_list[j], LHH_list[j], HHH_list[j])

        return self.mse_loss(x, LLL), bits, LLL



        # print("Writting...")
        # f_txt = open('/output/LLL_test.txt', 'w')
        # for i in LLL[0, 0, 2, 2, :]:
        #     f_txt.write(str(i))
        #     f_txt.write('\t')
        # f_txt.close()

        # for i in range((anchor2_sequeeze(LLL).size(2))):
        #     for j in range((anchor2_sequeeze(LLL).size(3))):
        #         for k in range((anchor2_sequeeze(LLL).size(4))):
        #             print(i,j,k, anchor2_sequeeze(LLL)[0,0,i,j,k])
        # exit()