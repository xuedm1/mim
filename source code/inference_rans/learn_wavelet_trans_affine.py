import torch
from torch.nn import functional as F

lifting_coeff = [-1.586134342059924, -0.052980118572961, 0.882911075530934, 0.443506852043971, 0.869864451624781, 1.149604398860241] # bior4.4


class P_block(torch.nn.Module):
    def __init__(self):
        super(P_block, self).__init__()

        self.conv1 = torch.nn.Conv3d(1, 16, 3, 1, 0)  # 没有初始化
        self.conv2 = torch.nn.Conv3d(16, 16, 3, 1, 0)
        self.conv3 = torch.nn.Conv3d(16, 16, 3, 1, 0)
        self.conv4 = torch.nn.Conv3d(16, 1, 3, 1, 0)
        self.act = torch.nn.ReLU()

        self.conv_post = torch.nn.Conv3d(1, 1, 3, 1, 0)
        self.conv_logscale_for_affine = torch.nn.Conv3d(1, 1, 3, 1, 0)

        torch.nn.init.zeros_(self.conv_logscale_for_affine.weight)
        torch.nn.init.ones_(self.conv_logscale_for_affine.bias)


    def forward(self, x):
        x1 = self.conv1(self.pad_3d(x))
        x2 = self.conv2(self.pad_3d(self.act(x1)))
        x3 = self.conv3(self.pad_3d(self.act(x2)))
        x4 = x1 + x3
        x5 = self.conv4(self.pad_3d(x4))
        shift = self.conv_post(self.pad_3d(x5))
        scale = self.conv_logscale_for_affine(self.pad_3d(x5))
        return shift, torch.exp(scale/65535.)

    def pad_3d(self, x):
        paddings = (1, 1, 1, 1, 1, 1)
        tmp = F.pad(x, paddings, "circular")
        return tmp



class learn_lifting97(torch.nn.Module):
    def __init__(self, trainable_set):
        super(learn_lifting97, self).__init__()
        if trainable_set:
            self.leran_wavelet_rate = 0.1
        else:
            self.leran_wavelet_rate = 0.0
        self.skip1 = torch.nn.Conv3d(1, 1, (3, 1, 1, 1, 1), stride=1, padding=0, bias=False)
        self.skip1.weight = torch.nn.Parameter(torch.Tensor([[[[[0.0], [lifting_coeff[0]], [lifting_coeff[0]]]]]]),
                                               requires_grad=False)
        self.p_block1 = P_block()

        self.skip2 = torch.nn.Conv3d(1, 1, (3, 1, 1, 1, 1), stride=1, padding=0, bias=False)
        self.skip2.weight = torch.nn.Parameter(torch.Tensor([[[[[lifting_coeff[1]], [lifting_coeff[1]], [0.0]]]]]),
                                               requires_grad=False)
        self.p_block2 = P_block()

        self.skip3 = torch.nn.Conv3d(1, 1, (3, 1, 1, 1, 1), stride=1, padding=0, bias=False)
        self.skip3.weight = torch.nn.Parameter(torch.Tensor([[[[[0.0], [lifting_coeff[2]], [lifting_coeff[2]]]]]]),
                                               requires_grad=False)
        self.p_block3 = P_block()

        self.skip4 = torch.nn.Conv3d(1, 1, (3, 1, 1, 1, 1), stride=1, padding=0, bias=False)
        self.skip4.weight = torch.nn.Parameter(torch.Tensor([[[[[lifting_coeff[3]], [lifting_coeff[3]], [0.0]]]]]),
                                               requires_grad=False)
        self.p_block4 = P_block()

        self.n_h = 0.0
        self.n_l = 0.0

    def forward_trans(self, L, H):

        paddings = (0, 0, 1, 1, 0, 0)

        tmp = F.pad(L, paddings, "circular")
        skip1 = self.skip1(tmp)
        L_net, scale_for_affine = self.p_block1(skip1)
        H = H + skip1 + L_net * self.leran_wavelet_rate
        H = H * scale_for_affine

        tmp = F.pad(H, paddings, "circular")
        skip2 = self.skip2(tmp)
        H_net, scale_for_affine = self.p_block2(skip2)
        L = L + skip2 + H_net * self.leran_wavelet_rate
        L = L * scale_for_affine

        tmp = F.pad(L, paddings, "circular")
        skip3 = self.skip3(tmp)
        L_net, scale_for_affine = self.p_block3(skip3)
        H = H + skip3 + L_net * self.leran_wavelet_rate
        H = H*scale_for_affine

        tmp = F.pad(H, paddings, "circular")
        skip4 = self.skip4(tmp)
        H_net, scale_for_affine = self.p_block4(skip4)
        L = L + skip4 + H_net * self.leran_wavelet_rate
        L = L*scale_for_affine

        H = H * (lifting_coeff[4] + self.n_h * self.leran_wavelet_rate)
        L = L * (lifting_coeff[5] + self.n_l * self.leran_wavelet_rate)

        return L, H

    def inverse_trans(self, L, H):

        H = H / (lifting_coeff[4] + self.n_h * self.leran_wavelet_rate)
        L = L / (lifting_coeff[5] + self.n_l * self.leran_wavelet_rate)

        paddings = (0, 0, 1, 1, 0, 0)

        tmp = F.pad(H, paddings, "circular")
        skip4 = self.skip4(tmp)
        H_net, scale_for_affine = self.p_block4(skip4)
        L = L/scale_for_affine
        L = L - skip4 - H_net * self.leran_wavelet_rate

        tmp = F.pad(L, paddings, "circular")
        skip3 = self.skip3(tmp)
        L_net, scale_for_affine = self.p_block3(skip3)
        H = H / scale_for_affine
        H = H - skip3 - L_net * self.leran_wavelet_rate

        tmp = F.pad(H, paddings, "circular")
        skip2 = self.skip2(tmp)
        H_net, scale_for_affine = self.p_block2(skip2)
        L = L/scale_for_affine
        L = L - skip2 - H_net * self.leran_wavelet_rate

        tmp = F.pad(L, paddings, "circular")
        skip1 = self.skip1(tmp)
        L_net, scale_for_affine = self.p_block1(skip1)
        H = H / scale_for_affine
        H = H - skip1 - L_net * self.leran_wavelet_rate

        return L, H

class Wavelet(torch.nn.Module):
    def __init__(self, trainable_set):
        super(Wavelet, self).__init__()
        
        self.lifting0 = learn_lifting97(trainable_set)
        self.lifting1 = learn_lifting97(trainable_set)
        self.lifting2 = learn_lifting97(trainable_set)
        self.lifting3 = learn_lifting97(trainable_set)
        self.lifting4 = learn_lifting97(trainable_set)
        self.lifting5 = learn_lifting97(trainable_set)
        self.lifting6 = learn_lifting97(trainable_set)

    def forward_trans(self, x):
        # transform for rows
        # 1
        L = x[:, :, 0::2, :, :]
        H = x[:, :, 1::2, :, :]
        L, H = self.lifting0.forward_trans(L, H)

        # 2
        L = L.permute(0, 1, 3, 2, 4)
        LL = L[:, :, 0::2, :, :]
        HL = L[:, :, 1::2, :, :]
        LL, HL = self.lifting1.forward_trans(LL, HL)
        LL = LL.permute(0, 1, 3, 2, 4)
        HL = HL.permute(0, 1, 3, 2, 4)

        H = H.permute(0, 1, 3, 2, 4)
        LH = H[:, :, 0::2, :, :]
        HH = H[:, :, 1::2, :, :]
        LH, HH = self.lifting2.forward_trans(LH, HH)
        LH = LH.permute(0, 1, 3, 2, 4)
        HH = HH.permute(0, 1, 3, 2, 4)

        # 3
        LL = LL.permute(0, 1, 4, 3, 2)
        LLL = LL[:, :, 0::2, :, :]
        HLL = LL[:, :, 1::2, :, :]
        LLL, HLL = self.lifting3.forward_trans(LLL, HLL)
        LLL = LLL.permute(0, 1, 4, 3, 2)
        HLL = HLL.permute(0, 1, 4, 3, 2)

        HL = HL.permute(0, 1, 4, 3, 2)
        LHL = HL[:, :, 0::2, :, :]
        HHL = HL[:, :, 1::2, :, :]
        LHL, HHL = self.lifting4.forward_trans(LHL, HHL)
        LHL = LHL.permute(0, 1, 4, 3, 2)
        HHL = HHL.permute(0, 1, 4, 3, 2)

        LH = LH.permute(0, 1, 4, 3, 2)
        LLH = LH[:, :, 0::2, :, :]
        HLH = LH[:, :, 1::2, :, :]
        LLH, HLH = self.lifting5.forward_trans(LLH, HLH)
        LLH = LLH.permute(0, 1, 4, 3, 2)
        HLH = HLH.permute(0, 1, 4, 3, 2)

        HH = HH.permute(0, 1, 4, 3, 2)
        LHH = HH[:, :, 0::2, :, :]
        HHH = HH[:, :, 1::2, :, :]
        LHH, HHH = self.lifting6.forward_trans(LHH, HHH)
        LHH = LHH.permute(0, 1, 4, 3, 2)
        HHH = HHH.permute(0, 1, 4, 3, 2)

        return LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH

    def inverse_trans(self, LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH):
        # 3
        LLL = LLL.permute(0, 1, 4, 3, 2)
        HLL = HLL.permute(0, 1, 4, 3, 2)
        LL = torch.zeros(LLL.size()[0], LLL.size()[1], LLL.size()[2] + HLL.size()[2], LLL.size()[3], LLL.size()[4],
                         device="cuda")
        LLL, HLL = self.lifting6.inverse_trans(LLL, HLL)
        LL[:, :, 0::2, :, :] = LLL
        LL[:, :, 1::2, :, :] = HLL
        LL = LL.permute(0, 1, 4, 3, 2)

        LHL = LHL.permute(0, 1, 4, 3, 2)
        HHL = HHL.permute(0, 1, 4, 3, 2)
        HL = torch.zeros(LHL.size()[0], LHL.size()[1], LHL.size()[2] + HHL.size()[2], LHL.size()[3], LHL.size()[4],
                         device="cuda")
        LHL, HHL = self.lifting5.inverse_trans(LHL, HHL)
        HL[:, :, 0::2, :, :] = LHL
        HL[:, :, 1::2, :, :] = HHL
        HL = HL.permute(0, 1, 4, 3, 2)

        LLH = LLH.permute(0, 1, 4, 3, 2)
        HLH = HLH.permute(0, 1, 4, 3, 2)
        LH = torch.zeros(LLH.size()[0], LLH.size()[1], LLH.size()[2] + HLH.size()[2], LLH.size()[3], LLH.size()[4],
                         device="cuda")
        LLH, HLH = self.lifting4.inverse_trans(LLH, HLH)
        LH[:, :, 0::2, :, :] = LLH
        LH[:, :, 1::2, :, :] = HLH
        LH = LH.permute(0, 1, 4, 3, 2)

        LHH = LHH.permute(0, 1, 4, 3, 2)
        HHH = HHH.permute(0, 1, 4, 3, 2)
        HH = torch.zeros(LHH.size()[0], LHH.size()[1], LHH.size()[2] + HHH.size()[2], LHH.size()[3], LHH.size()[4],
                         device="cuda")
        LHH, HHH = self.lifting3.inverse_trans(LHH, HHH)
        HH[:, :, 0::2, :, :] = LHH
        HH[:, :, 1::2, :, :] = HHH
        HH = HH.permute(0, 1, 4, 3, 2)

        # 2
        LH = LH.permute(0, 1, 3, 2, 4)
        HH = HH.permute(0, 1, 3, 2, 4)
        H = torch.zeros(LH.size()[0], LH.size()[1], LH.size()[2] + HH.size()[2], LH.size()[3], LH.size()[4],
                        device="cuda")
        LH, HH = self.lifting2.inverse_trans(LH, HH)
        H[:, :, 0::2, :, :] = LH
        H[:, :, 1::2, :, :] = HH
        H = H.permute(0, 1, 3, 2, 4)

        LL = LL.permute(0, 1, 3, 2, 4)
        HL = HL.permute(0, 1, 3, 2, 4)
        L = torch.zeros(LL.size()[0], LL.size()[1], LL.size()[2] + HL.size()[2], LL.size()[3], LL.size()[4],
                        device="cuda")
        LL, HL = self.lifting1.inverse_trans(LL, HL)
        L[:, :, 0::2, :, :] = LL
        L[:, :, 1::2, :, :] = HL
        L = L.permute(0, 1, 3, 2, 4)

        # 1
        L, H = self.lifting0.inverse_trans(L, H)
        x = torch.zeros(L.size()[0], L.size()[1], L.size()[2] + H.size()[2], L.size()[3], L.size()[4], device="cuda")
        x[:, :, 0::2, :, :] = L
        x[:, :, 1::2, :, :] = H

        return x
