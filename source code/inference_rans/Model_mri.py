import torch
import sys
import time
import torch.nn as nn
import torch.nn.functional as F

import learn_wavelet_trans_additive
import learn_wavelet_trans_affine
import learn_wavelet_trans_lossless
import PixelCNN_light5 as PixelCNN
import unet as Unet
import Quant
from gan_post import GANPostProcessing
from rcan import RCAN as PostProcessing


def Get_high_bits(x):
    out = []
    for i in x:
        MSB = (i >> 8) & 0xFF # 高8位
        out.append(MSB)
    return out


def Get_low_bits(x):
    out = []
    for i in x:
        LSB = i & 0xFF # 低8位
        out.append(LSB)
    return out


def Get_sign(ori):
    out1 = []
    for ori_each in ori:
        sign = torch.ones_like(ori_each)
        sign[ori_each<0] = -1
        out1.append(sign)
    return out1


def Get_abs(x):
    out = []
    for i in x:
        i = torch.abs(i)
        out.append(i)
    return out
            

def Recon(x_l, x_h, x_s):

    reconstructed_data = ((x_h << 8) | x_l) * x_s
    
    return reconstructed_data


class Transform_lossless(torch.nn.Module):
    def __init__(self, trainable_set=False):
        super(Transform_lossless, self).__init__()

        self.trans_steps = 4

        self.trainable_set = trainable_set

        self.wavelet_transform_53 = learn_wavelet_trans_lossless.Wavelet()

    def forward_trans(self, x):
        LLL = x
        HLL_list = []
        LHL_list = []
        HHL_list = []
        LLH_list = []
        HLH_list = []
        LHH_list = []
        HHH_list = []

        LLL_list_h = []
        HLL_list_h = []
        LHL_list_h = []
        HHL_list_h = []
        LLH_list_h = []
        HLH_list_h = []
        LHH_list_h = []
        HHH_list_h = []

        LLL_list_s = []
        HLL_list_s = []
        LHL_list_s = []
        HHL_list_s = []
        LLH_list_s = []
        HLH_list_s = []
        LHH_list_s = []
        HHH_list_s = []

        for i in range(self.trans_steps):
            LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH = self.wavelet_transform_53.forward_trans(LLL)
            
            [LLL_s, HLL_s, LHL_s, HHL_s, LLH_s, HLH_s, LHH_s, HHH_s] = Get_sign([LLL.type(torch.int32), HLL.type(torch.int32), LHL.type(torch.int32), HHL.type(torch.int32), LLH.type(torch.int32), HLH.type(torch.int32), LHH.type(torch.int32), HHH.type(torch.int32)])
            [LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH] = Get_abs([LLL.type(torch.int32), HLL.type(torch.int32), LHL.type(torch.int32), HHL.type(torch.int32), LLH.type(torch.int32), HLH.type(torch.int32), LHH.type(torch.int32), HHH.type(torch.int32)])

            [LLL_h, HLL_h, LHL_h, HHL_h, LLH_h, HLH_h, LHH_h, HHH_h] = Get_high_bits([LLL.type(torch.int32), HLL.type(torch.int32), LHL.type(torch.int32), HHL.type(torch.int32), LLH.type(torch.int32), HLH.type(torch.int32), LHH.type(torch.int32), HHH.type(torch.int32)])
            [LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH] = Get_low_bits([LLL.type(torch.int32), HLL.type(torch.int32), LHL.type(torch.int32), HHL.type(torch.int32), LLH.type(torch.int32), HLH.type(torch.int32), LHH.type(torch.int32), HHH.type(torch.int32)])
            
            HLL_list.append(HLL)
            LHL_list.append(LHL)
            HHL_list.append(HHL)
            LLH_list.append(LLH)
            HLH_list.append(HLH)
            LHH_list.append(LHH)
            HHH_list.append(HHH)

            LLL_list_h.append(LLL_h)
            HLL_list_h.append(HLL_h)
            LHL_list_h.append(LHL_h)
            HHL_list_h.append(HHL_h)
            LLH_list_h.append(LLH_h)
            HLH_list_h.append(HLH_h)
            LHH_list_h.append(LHH_h)
            HHH_list_h.append(HHH_h)

            LLL_list_s.append(LLL_s)
            HLL_list_s.append(HLL_s)
            LHL_list_s.append(LHL_s)
            HHL_list_s.append(HHL_s)
            LLH_list_s.append(LLH_s)
            HLH_list_s.append(HLH_s)
            LHH_list_s.append(LHH_s)
            HHH_list_s.append(HHH_s)

        return LLL, HLL_list, LHL_list, HHL_list, LLH_list, HLH_list, LHH_list, HHH_list, LLL_list_h, HLL_list_h, LHL_list_h, HHL_list_h, LLH_list_h, HLH_list_h, LHH_list_h, HHH_list_h, LLL_list_s, HLL_list_s, LHL_list_s, HHL_list_s, LLH_list_s, HLH_list_s, LHH_list_s, HHH_list_s

    def inverse_trans(self, LLL, HLL_list, LHL_list, HHL_list, LLH_list, HLH_list, LHH_list, HHH_list):

        LLL = self.wavelet_transform_53.inverse_trans(LLL, HLL_list, LHL_list, HHL_list, LLH_list, HLH_list, LHH_list, HHH_list)

        return LLL


def conv3x3(in_ch, out_ch, stride=1):
    """3x3 convolution with padding."""
    return nn.Conv3d(in_ch, out_ch, kernel_size=3, stride=stride, padding=1)


class ResidualBlock(nn.Module):
    """Simple residual block with two 3x3 convolutions.

    Args:
        in_ch (int): number of input channels
        out_ch (int): number of output channels
    """

    def __init__(self, in_ch, out_ch, leaky_relu_slope=0.01, inplace=False):
        super().__init__()
        self.conv1 = conv3x3(in_ch, out_ch)
        self.leaky_relu = nn.LeakyReLU(negative_slope=leaky_relu_slope, inplace=inplace)
        self.conv2 = conv3x3(out_ch, out_ch)
        self.adaptor = None
        if in_ch != out_ch:
            self.adaptor = conv1x1(in_ch, out_ch)

    def forward(self, x):
        identity = x
        if self.adaptor is not None:
            identity = self.adaptor(identity)

        out = self.conv1(x)
        out = self.leaky_relu(out)
        out = self.conv2(out)
        out = self.leaky_relu(out)

        out = out + identity
        return out


class ResidualBlockWithStride2(nn.Module):
    def __init__(self, in_ch, out_ch, stride=2, inplace=False):
        super().__init__()
        self.down = nn.Conv3d(in_ch, out_ch, 2, stride=2)
        self.conv = nn.Sequential(
            nn.Conv3d(out_ch, out_ch, 3, padding=1),
            nn.LeakyReLU(inplace=inplace),
            nn.Conv3d(out_ch, out_ch, 1),
            nn.LeakyReLU(inplace=inplace),
        )

    def forward(self, x):
        x = self.down(x)
        return x


class ResidualBlockWithStride2_z(nn.Module):
    def __init__(self, in_ch, out_ch, stride=(1,2,2), inplace=False):
        super().__init__()
        self.down = nn.Conv3d(in_ch, out_ch, 3, stride=(1,2,2), padding=(1,1,1))
        self.conv = nn.Sequential(
            nn.Conv3d(out_ch, out_ch, 3, padding=1),
            nn.LeakyReLU(inplace=inplace),
            nn.Conv3d(out_ch, out_ch, 1),
            nn.LeakyReLU(inplace=inplace),
        )

    def forward(self, x):
        x = self.down(x)
        identity = x
        out = self.conv(x)
        out = out + identity
        return x


class UnetModel(nn.Module):

    def __init__(self, in_channels, out_channels, model_depth=4, final_activation="sigmoid"):
        super(UnetModel, self).__init__()

        self.block0 = ResidualBlock(1,1)
        self.block1 = ResidualBlockWithStride2(1,1)
        self.block2 = ResidualBlockWithStride2(1,1)
        self.block3 = ResidualBlockWithStride2(1,1)
        self.block4 = ResidualBlockWithStride2(1,1)
        self.block5 = ResidualBlockWithStride2_z(1,1)

    def forward(self, x):

        x = self.block0(x)
        x_1 = self.block5(x)
        x_2 = self.block1(x_1)
        x_3 = self.block2(x_2)
        x_4 = self.block3(x_3)
        x_5 = self.block4(x_4)

        return [x_2, x_3, x_4, x_5]


class high_generator(nn.Module):

    def __init__(self):
        super(high_generator, self).__init__()

        self.Highbit_Feature = Unet.UnetModel(1, 1)

    def forward(self, x):

        feature_high = self.Highbit_Feature(x) 

        return feature_high



class CodingLLL_lossless_1(torch.nn.Module):
    def __init__(self):
        super(CodingLLL_lossless_1, self).__init__()

        self.coding_LLL_1 = PixelCNN.PixelCNN(2)

    def forward(self, LLL, context, lower_bound, upper_bound):
        prob = self.coding_LLL_1(LLL, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound):
        prob = self.coding_LLL_1.inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingLLL_lossless_2(torch.nn.Module):
    def __init__(self):
        super(CodingLLL_lossless_2, self).__init__()

        self.coding_LLL_2 = PixelCNN.PixelCNN(3)

    def forward(self, LLL, context, lower_bound, upper_bound):
        prob = self.coding_LLL_2(LLL, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound):
        prob = self.coding_LLL_2.inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingLLL_lossless_3(torch.nn.Module):
    def __init__(self):
        super(CodingLLL_lossless_3, self).__init__()

        self.coding_LLL_3 = PixelCNN.PixelCNN(4)

    def forward(self, LLL, context, lower_bound, upper_bound):
        prob = self.coding_LLL_3(LLL, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound):
        prob = self.coding_LLL_3.inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingLLL_lossless_4(torch.nn.Module):
    def __init__(self):
        super(CodingLLL_lossless_4, self).__init__()

        self.coding_LLL_4 = PixelCNN.PixelCNN(5)

    def forward(self, LLL, context, lower_bound, upper_bound):
        prob = self.coding_LLL_4(LLL, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound):
        prob = self.coding_LLL_4.inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingHLL_lossless_1(torch.nn.Module):
    def __init__(self):
        super(CodingHLL_lossless_1, self).__init__()

        self.trans_steps = 4

        self.coding_HLL_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(6) for _i in range(self.trans_steps)])

    def forward(self, HLL, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLL_list_1[layer](HLL, context , lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLL_list_1[layer].inf_cdf_lower(context , lower_bound, upper_bound)
        return prob


class CodingHLL_lossless_2(torch.nn.Module):
    def __init__(self):
        super(CodingHLL_lossless_2, self).__init__()

        self.trans_steps = 4

        self.coding_HLL_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(7) for _i in range(self.trans_steps)])

    def forward(self, HLL, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLL_list_2[layer](HLL, context , lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLL_list_2[layer].inf_cdf_lower(context , lower_bound, upper_bound)
        return prob


class CodingHLL_lossless_3(torch.nn.Module):
    def __init__(self):
        super(CodingHLL_lossless_3, self).__init__()

        self.trans_steps = 4

        self.coding_HLL_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(8) for _i in range(self.trans_steps)])

    def forward(self, HLL, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLL_list_3[layer](HLL, context , lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLL_list_3[layer].inf_cdf_lower(context , lower_bound, upper_bound)
        return prob


class CodingHLL_lossless_4(torch.nn.Module):
    def __init__(self):
        super(CodingHLL_lossless_4, self).__init__()

        self.trans_steps = 4

        self.coding_HLL_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(9) for _i in range(self.trans_steps)])

    def forward(self, HLL, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLL_list_4[layer](HLL, context , lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLL_list_4[layer].inf_cdf_lower(context , lower_bound, upper_bound)
        return prob


class CodingLHL_lossless_1(torch.nn.Module):
    def __init__(self):
        super(CodingLHL_lossless_1, self).__init__()

        self.trans_steps = 4

        self.coding_LHL_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(10) for _i in range(self.trans_steps)])

    def forward(self, LHL, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHL_list_1[layer](LHL, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHL_list_1[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingLHL_lossless_2(torch.nn.Module):
    def __init__(self):
        super(CodingLHL_lossless_2, self).__init__()

        self.trans_steps = 4

        self.coding_LHL_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(11) for _i in range(self.trans_steps)])

    def forward(self, LHL, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHL_list_2[layer](LHL, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHL_list_2[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingLHL_lossless_3(torch.nn.Module):
    def __init__(self):
        super(CodingLHL_lossless_3, self).__init__()

        self.trans_steps = 4

        self.coding_LHL_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(12) for _i in range(self.trans_steps)])

    def forward(self, LHL, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHL_list_3[layer](LHL, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHL_list_3[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingLHL_lossless_4(torch.nn.Module):
    def __init__(self):
        super(CodingLHL_lossless_4, self).__init__()

        self.trans_steps = 4

        self.coding_LHL_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(13) for _i in range(self.trans_steps)])

    def forward(self, LHL, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHL_list_4[layer](LHL, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHL_list_4[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingHHL_lossless_1(torch.nn.Module):
    def __init__(self):
        super(CodingHHL_lossless_1, self).__init__()

        self.trans_steps = 4

        self.coding_HHL_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(14) for _i in range(self.trans_steps)])

    def forward(self, HHL, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHL_list_1[layer](HHL, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHL_list_1[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingHHL_lossless_2(torch.nn.Module):
    def __init__(self):
        super(CodingHHL_lossless_2, self).__init__()

        self.trans_steps = 4

        self.coding_HHL_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(15) for _i in range(self.trans_steps)])

    def forward(self, HHL, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHL_list_2[layer](HHL, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHL_list_2[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingHHL_lossless_3(torch.nn.Module):
    def __init__(self):
        super(CodingHHL_lossless_3, self).__init__()

        self.trans_steps = 4

        self.coding_HHL_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(16) for _i in range(self.trans_steps)])

    def forward(self, HHL, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHL_list_3[layer](HHL, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHL_list_3[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingHHL_lossless_4(torch.nn.Module):
    def __init__(self):
        super(CodingHHL_lossless_4, self).__init__()

        self.trans_steps = 4

        self.coding_HHL_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(17) for _i in range(self.trans_steps)])

    def forward(self, HHL, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHL_list_4[layer](HHL, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHL_list_4[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingLLH_lossless_1(torch.nn.Module):
    def __init__(self):
        super(CodingLLH_lossless_1, self).__init__()

        self.trans_steps = 4

        self.coding_LLH_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(18) for _i in range(self.trans_steps)])

    def forward(self, LLH, context, lower_bound, upper_bound, layer):
        prob = self.coding_LLH_list_1[layer](LLH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_LLH_list_1[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingLLH_lossless_2(torch.nn.Module):
    def __init__(self):
        super(CodingLLH_lossless_2, self).__init__()

        self.trans_steps = 4

        self.coding_LLH_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(19) for _i in range(self.trans_steps)])

    def forward(self, LLH, context, lower_bound, upper_bound, layer):
        prob = self.coding_LLH_list_2[layer](LLH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_LLH_list_2[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingLLH_lossless_3(torch.nn.Module):
    def __init__(self):
        super(CodingLLH_lossless_3, self).__init__()

        self.trans_steps = 4

        self.coding_LLH_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(20) for _i in range(self.trans_steps)])

    def forward(self, LLH, context, lower_bound, upper_bound, layer):
        prob = self.coding_LLH_list_3[layer](LLH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_LLH_list_3[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingLLH_lossless_4(torch.nn.Module):
    def __init__(self):
        super(CodingLLH_lossless_4, self).__init__()

        self.trans_steps = 4

        self.coding_LLH_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(21) for _i in range(self.trans_steps)])

    def forward(self, LLH, context, lower_bound, upper_bound, layer):
        prob = self.coding_LLH_list_4[layer](LLH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_LLH_list_4[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingHLH_lossless_1(torch.nn.Module):
    def __init__(self):
        super(CodingHLH_lossless_1, self).__init__()

        self.trans_steps = 4

        self.coding_HLH_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(22) for _i in range(self.trans_steps)])

    def forward(self, HLH, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLH_list_1[layer](HLH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLH_list_1[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingHLH_lossless_2(torch.nn.Module):
    def __init__(self):
        super(CodingHLH_lossless_2, self).__init__()

        self.trans_steps = 4

        self.coding_HLH_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(23) for _i in range(self.trans_steps)])

    def forward(self, HLH, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLH_list_2[layer](HLH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLH_list_2[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingHLH_lossless_3(torch.nn.Module):
    def __init__(self):
        super(CodingHLH_lossless_3, self).__init__()

        self.trans_steps = 4

        self.coding_HLH_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(24) for _i in range(self.trans_steps)])

    def forward(self, HLH, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLH_list_3[layer](HLH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLH_list_3[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingHLH_lossless_4(torch.nn.Module):
    def __init__(self):
        super(CodingHLH_lossless_4, self).__init__()

        self.trans_steps = 4

        self.coding_HLH_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(25) for _i in range(self.trans_steps)])

    def forward(self, HLH, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLH_list_4[layer](HLH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HLH_list_4[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingLHH_lossless_1(torch.nn.Module):
    def __init__(self):
        super(CodingLHH_lossless_1, self).__init__()

        self.trans_steps = 4

        self.coding_LHH_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(26) for _i in range(self.trans_steps)])

    def forward(self, LHH, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHH_list_1[layer](LHH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHH_list_1[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingLHH_lossless_2(torch.nn.Module):
    def __init__(self):
        super(CodingLHH_lossless_2, self).__init__()

        self.trans_steps = 4

        self.coding_LHH_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(27) for _i in range(self.trans_steps)])

    def forward(self, LHH, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHH_list_2[layer](LHH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHH_list_2[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingLHH_lossless_3(torch.nn.Module):
    def __init__(self):
        super(CodingLHH_lossless_3, self).__init__()

        self.trans_steps = 4

        self.coding_LHH_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(28) for _i in range(self.trans_steps)])

    def forward(self, LHH, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHH_list_3[layer](LHH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHH_list_3[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingLHH_lossless_4(torch.nn.Module):
    def __init__(self):
        super(CodingLHH_lossless_4, self).__init__()

        self.trans_steps = 4

        self.coding_LHH_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(29) for _i in range(self.trans_steps)])

    def forward(self, LHH, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHH_list_4[layer](LHH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_LHH_list_4[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingHHH_lossless_1(torch.nn.Module):
    def __init__(self):
        super(CodingHHH_lossless_1, self).__init__()

        self.trans_steps = 4

        self.coding_HHH_list_1 = torch.nn.ModuleList([PixelCNN.PixelCNN(30) for _i in range(self.trans_steps)])

    def forward(self, HHH, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHH_list_1[layer](HHH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHH_list_1[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingHHH_lossless_2(torch.nn.Module):
    def __init__(self):
        super(CodingHHH_lossless_2, self).__init__()

        self.trans_steps = 4

        self.coding_HHH_list_2 = torch.nn.ModuleList([PixelCNN.PixelCNN(31) for _i in range(self.trans_steps)])

    def forward(self, HHH, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHH_list_2[layer](HHH, context, lower_bound, upper_bound)
        return prob
        
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHH_list_2[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingHHH_lossless_3(torch.nn.Module):
    def __init__(self):
        super(CodingHHH_lossless_3, self).__init__()

        self.trans_steps = 4

        self.coding_HHH_list_3 = torch.nn.ModuleList([PixelCNN.PixelCNN(32) for _i in range(self.trans_steps)])

    def forward(self, HHH, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHH_list_3[layer](HHH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHH_list_3[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class CodingHHH_lossless_4(torch.nn.Module):
    def __init__(self):
        super(CodingHHH_lossless_4, self).__init__()

        self.trans_steps = 4

        self.coding_HHH_list_4 = torch.nn.ModuleList([PixelCNN.PixelCNN(33) for _i in range(self.trans_steps)])

    def forward(self, HHH, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHH_list_4[layer](HHH, context, lower_bound, upper_bound)
        return prob
    
    def inf_cdf_lower(self, context, lower_bound, upper_bound, layer):
        prob = self.coding_HHH_list_4[layer].inf_cdf_lower(context, lower_bound, upper_bound)
        return prob


class Transform_aiWave(torch.nn.Module):
    def __init__(self, scale, trainable_set=True):
        super(Transform_aiWave, self).__init__()

        self.trans_steps = 4
        self.trainable_set = trainable_set
        self.wavelet_transform = learn_wavelet_trans_additive.Wavelet(True)

        self.scale_net = Quant.Scale_net()
        self.quant = Quant.Quant()
        self.dequant = Quant.DeQuant()

        self.scale_init = scale

    def forward_trans(self, x):

        self.scale = bool(self.trainable_set) * self.scale_net() + self.scale_init

        LLL = x
        HLL_list = []
        LHL_list = []
        HHL_list = []
        LLH_list = []
        HLH_list = []
        LHH_list = []
        HHH_list = []

        for i in range(self.trans_steps):
            LLL, HLL, LHL, HHL, LLH, HLH, LHH, HHH = self.wavelet_transform.forward_trans(LLL)

            HLL_list.append(torch.round(HLL / self.scale))
            LHL_list.append(torch.round(LHL / self.scale))
            HHL_list.append(torch.round(HHL / self.scale))
            LLH_list.append(torch.round(LLH / self.scale))
            HLH_list.append(torch.round(HLH / self.scale))
            LHH_list.append(torch.round(LHH / self.scale))
            HHH_list.append(torch.round(HHH / self.scale))
        LLL = torch.round(LLL / self.scale)

        return LLL, HLL_list, LHL_list, HHL_list, LLH_list, HLH_list, LHH_list, HHH_list, self.scale

    def inverse_trans(self, LLL, HLL_list, LHL_list, HHL_list, LLH_list, HLH_list, LHH_list, HHH_list):

        LLL = self.wavelet_transform.inverse_trans(LLL, HLL_list, LHL_list, HHL_list, LLH_list, HLH_list, LHH_list, HHH_list)

        return LLL

    def used_scale(self):
        scale = float(self.trainable_set) * self.scale_net() + self.scale_init
        return scale


class Post(torch.nn.Module):
    def __init__(self):
        super(Post, self).__init__()

        self.post = PostProcessing(n_resgroups=2, n_resblocks=2, n_feats=32)

    def forward(self, x):

        post_recon = self.post(x)

        return post_recon

