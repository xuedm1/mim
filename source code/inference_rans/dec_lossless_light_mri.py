import numpy as np
import torch
import copy
import time
from PIL import Image
from torch.autograd import Variable
from torch.nn import functional as F

import rans
import Model_mri
from utils import (find_min_and_max_4, img2patch, img2patch_padding, patch2img)
import SimpleITK as sitk

PRECISION = 16 
FACTOR = torch.tensor(2, dtype=torch.float32).pow_(PRECISION)
def to_variable(x):
    if torch.cuda.is_available():
        x = x.cuda()
    return Variable(x)


def dec_binary(ans_dec, bin_num):
    value = 0
    quantized_cdf = [0,32768,65536]
    for i in range(bin_num):
        dec_c = ans_dec.decode(quantized_cdf)
        value = value + (2**(bin_num-1-i))*dec_c
    return value

def load_nii(path):
    nii = sitk.ReadImage(path)
    nii = sitk.GetArrayFromImage(nii)
    return nii


def save_nii(img, path):
    img = sitk.GetImageFromArray(img)
    sitk.WriteImage(img, path)


def anchor_mask(out):

    out[:, :, 0::2, 1::2, 0::2] = 0
    out[:, :, 0::2, 0::2, 1::2] = 0
    out[:, :, 1::2, 1::2, 1::2] = 0
    out[:, :, 1::2, 0::2, 0::2] = 0

    return out


def de_anchor_unsequeeze1(anchor):
    B, C, Z, H, W = anchor.shape
    y_anchor = torch.zeros([B, C, Z, H * 2, W * 2], device=anchor.device)
    y_anchor[:, :, 0::2, 0::2, 0::2] = anchor[:, :, 0::2, :, :]
    y_anchor[:, :, 1::2, 1::2, 1::2] = anchor[:, :, 1::2, :, :]

    return y_anchor


def de_anchor_unsequeeze2(anchor):
    B, C, Z, H, W = anchor.shape
    y_anchor = torch.zeros([B, C, Z, H * 2, W * 2], device=anchor.device)
    y_anchor[:, :, 0::2, 1::2, 1::2] = anchor[:, :, 0::2, :, :]
    y_anchor[:, :, 1::2, 0::2, 0::2] = anchor[:, :, 1::2, :, :]

    return y_anchor


def de_anchor_unsequeeze3(anchor):
    B, C, Z, H, W = anchor.shape
    y_anchor = torch.zeros([B, C, Z, H * 2, W * 2], device=anchor.device)
    y_anchor[:, :, 0::2, 0::2, 1::2] = anchor[:, :, 0::2, :, :]
    y_anchor[:, :, 1::2, 1::2, 0::2] = anchor[:, :, 1::2, :, :]

    return y_anchor


def de_anchor_unsequeeze4(anchor):
    B, C, Z, H, W = anchor.shape
    y_anchor = torch.zeros([B, C, Z, H * 2, W * 2], device=anchor.device)
    y_anchor[:, :, 0::2, 1::2, 0::2] = anchor[:, :, 0::2, :, :]
    y_anchor[:, :, 1::2, 0::2, 1::2] = anchor[:, :, 1::2, :, :]

    return y_anchor


def anchor1_sequeeze(y):
    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H // 2, W // 2], device=y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 0::2, 0::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 1::2, 1::2]

    return anchor


def anchor2_sequeeze(y):
    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H // 2, W // 2], device=y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 1::2, 1::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 0::2, 0::2]

    return anchor


def anchor3_sequeeze(y):
    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H // 2, W // 2], device=y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 0::2, 1::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 1::2, 0::2]

    return anchor


def anchor4_sequeeze(y):
    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H // 2, W // 2], device=y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 1::2, 0::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 0::2, 1::2]

    return anchor


def concat1(x):
    out = torch.cat((anchor1_sequeeze(x), anchor2_sequeeze(x)), 1)
    return out

def concat2(x):
    out = torch.cat((anchor1_sequeeze(x), anchor2_sequeeze(x), anchor3_sequeeze(x)), 1)
    return out

def concat3(x):
    out = torch.cat((anchor1_sequeeze(x), anchor2_sequeeze(x), anchor3_sequeeze(x), anchor4_sequeeze(x)), 1)
    return out

def concat_(x1, x2):
    out = torch.cat((x1, x2), 1)
    return out

def concat1_(x1, x2, x3):
    out = torch.cat((x1, x2, x3), 1)
    return out

def Recon(x_l, x_h, x_s):

    reconstructed_data = ((x_h << 8) | x_l) * x_s
    
    return reconstructed_data


def dec_lossless(args, bin_name, dec, logfile):

    trans_steps = 4

    with torch.no_grad():

        model_path = args.model_path
        print(model_path)
        checkpoint = torch.load(model_path)

        all_part_dict = checkpoint['state_dict']

        models_dict = {}
        models_dict['transform'] = Model_mri.Transform_lossless()
        models_dict['high_generator'] = Model_mri.high_generator()
        models_dict['entropy_LLL_1'] = Model_mri.CodingLLL_lossless_1()
        models_dict['entropy_LLL_2'] = Model_mri.CodingLLL_lossless_2()
        models_dict['entropy_LLL_3'] = Model_mri.CodingLLL_lossless_3()
        models_dict['entropy_LLL_4'] = Model_mri.CodingLLL_lossless_4()

        models_dict['entropy_HLL_1'] = Model_mri.CodingHLL_lossless_1()
        models_dict['entropy_HLL_2'] = Model_mri.CodingHLL_lossless_2()
        models_dict['entropy_HLL_3'] = Model_mri.CodingHLL_lossless_3()
        models_dict['entropy_HLL_4'] = Model_mri.CodingHLL_lossless_4()

        models_dict['entropy_LHL_1'] = Model_mri.CodingLHL_lossless_1()
        models_dict['entropy_LHL_2'] = Model_mri.CodingLHL_lossless_2()
        models_dict['entropy_LHL_3'] = Model_mri.CodingLHL_lossless_3()
        models_dict['entropy_LHL_4'] = Model_mri.CodingLHL_lossless_4()

        models_dict['entropy_HHL_1'] = Model_mri.CodingHHL_lossless_1()
        models_dict['entropy_HHL_2'] = Model_mri.CodingHHL_lossless_2()
        models_dict['entropy_HHL_3'] = Model_mri.CodingHHL_lossless_3()
        models_dict['entropy_HHL_4'] = Model_mri.CodingHHL_lossless_4()

        models_dict['entropy_LLH_1'] = Model_mri.CodingLLH_lossless_1()
        models_dict['entropy_LLH_2'] = Model_mri.CodingLLH_lossless_2()
        models_dict['entropy_LLH_3'] = Model_mri.CodingLLH_lossless_3()
        models_dict['entropy_LLH_4'] = Model_mri.CodingLLH_lossless_4()

        models_dict['entropy_HLH_1'] = Model_mri.CodingHLH_lossless_1()
        models_dict['entropy_HLH_2'] = Model_mri.CodingHLH_lossless_2()
        models_dict['entropy_HLH_3'] = Model_mri.CodingHLH_lossless_3()
        models_dict['entropy_HLH_4'] = Model_mri.CodingHLH_lossless_4()

        models_dict['entropy_LHH_1'] = Model_mri.CodingLHH_lossless_1()
        models_dict['entropy_LHH_2'] = Model_mri.CodingLHH_lossless_2()
        models_dict['entropy_LHH_3'] = Model_mri.CodingLHH_lossless_3()
        models_dict['entropy_LHH_4'] = Model_mri.CodingLHH_lossless_4()

        models_dict['entropy_HHH_1'] = Model_mri.CodingHHH_lossless_1()
        models_dict['entropy_HHH_2'] = Model_mri.CodingHHH_lossless_2()
        models_dict['entropy_HHH_3'] = Model_mri.CodingHHH_lossless_3()
        models_dict['entropy_HHH_4'] = Model_mri.CodingHHH_lossless_4()

        models_dict_update = {}
        for key, model in models_dict.items():

            myparams_dict = model.state_dict()
            # print(myparams_dict.keys())
            all_part_dict_new = {}
            # print(checkpoint.keys())
            for k1,v1 in all_part_dict.items():
                new_k = k1.replace('module.', '') if 'module.' in k1 else k1
                all_part_dict_new[new_k] = v1
            # print(all_part_dict_new.keys())
            part_dict = {k: v for k, v in all_part_dict_new.items() if k in myparams_dict}
            # print(part_dict.keys())
            myparams_dict.update(part_dict)
            model.load_state_dict(myparams_dict)
            if torch.cuda.is_available():
                model = model.cuda()
                model.eval()
            models_dict_update[key] = model
        models_dict.update(models_dict_update)

        print('Load pre-trained model succeed!')
        logfile.write('Load pre-trained model succeed!' + '\n')
        logfile.flush()
        
        img_path = args.input_dir + '/' + args.img_name

        # start = time.time()

        print(img_path)
        logfile.write(img_path + '\n')
        logfile.flush()

        img = load_nii(img_path)
        img = np.array(img, dtype=np.float32)
        original_img = copy.deepcopy(img)

        img = torch.from_numpy(img)
        img = img.unsqueeze(0)
        img = img.unsqueeze(1)

        depth = dec_binary(dec, 15)
        height = dec_binary(dec, 15)
        width = dec_binary(dec, 15)

        pad_z = int(np.ceil(depth / 16)) * 16 - depth
        pad_h = int(np.ceil(height / 32)) * 32 - height
        pad_w = int(np.ceil(width / 32)) * 32 - width
        paddings = (0, pad_w, 0, pad_h, 0, pad_z)

        img = F.pad(img, paddings, 'constant')
        print("img_shape", img.shape)

        input_img_v = to_variable(img)
        
        LLL1, HLL_list1, LHL_list1, HHL_list1, LLH_list1, HLH_list1, LHH_list1, HHH_list1, LLL_list_h, HLL_list_h, LHL_list_h, HHL_list_h,LLH_list_h, HLH_list_h, LHH_list_h, HHH_list_h, LLL_list_s, HLL_list_s, LHL_list_s, HHL_list_s, LLH_list_s, HLH_list_s, LHH_list_s, HHH_list_s = models_dict[
'transform'].forward_trans(input_img_v)

        LLL = torch.zeros(1, 1, (depth + pad_z) // 16, (height + pad_h) // 16, (width + pad_w) // 16).cuda()
        HLL_list = []
        LHL_list = []
        HHL_list = []
        LLH_list = []
        HLH_list = []
        LHH_list = []
        HHH_list = []

        down_scales = [2, 4, 8, 16]
        for i in range(trans_steps):
            HLL_list.append(torch.zeros(1, 1, (depth + pad_z) // down_scales[i], (height + pad_h) // down_scales[i],
                                       (width + pad_w) // down_scales[i]).cuda())
            LHL_list.append(torch.zeros(1, 1, (depth + pad_z) // down_scales[i], (height + pad_h) // down_scales[i],
                                       (width + pad_w) // down_scales[i]).cuda())
            HHL_list.append(torch.zeros(1, 1, (depth + pad_z) // down_scales[i], (height + pad_h) // down_scales[i],
                                       (width + pad_w) // down_scales[i]).cuda())
            LLH_list.append(torch.zeros(1, 1, (depth + pad_z) // down_scales[i], (height + pad_h) // down_scales[i],
                                       (width + pad_w) // down_scales[i]).cuda())
            HLH_list.append(torch.zeros(1, 1, (depth + pad_z) // down_scales[i], (height + pad_h) // down_scales[i],
                                       (width + pad_w) // down_scales[i]).cuda())
            LHH_list.append(torch.zeros(1, 1, (depth + pad_z) // down_scales[i], (height + pad_h) // down_scales[i],
                                       (width + pad_w) // down_scales[i]).cuda())
            HHH_list.append(torch.zeros(1, 1, (depth + pad_z) // down_scales[i], (height + pad_h) // down_scales[i],
                                       (width + pad_w) // down_scales[i]).cuda())

        min_v = np.zeros(shape=(116), dtype=np.int)
        max_v = np.zeros(shape=(116), dtype=np.int)

        for j in range(116):
            min_v[j] = dec_binary(dec, 15) - 8000
            max_v[j] = dec_binary(dec, 15) - 8000

        # coded_coe_num = 0
            
        TEST_time1 = 0. # total time of entropy coding
        TEST_time2 = 0. # total time of building context
        TEST_time3 = 0. # total time of predicting probs
        TEST_time4 = 0. # total time of probs round and normalize
        TEST_time5 = 0. # total time of rans engine

        paddings = (2, 2, 2, 2, 2, 2)
        ref_padding = torch.nn.ReplicationPad3d(2)

        for i in range(1, 5):
            
            TEST_time_temp1 = time.time()
            TEST_time_temp2 = time.time()
            if i ==1: # decompress LLL_1
                a = min_v[0]
                b = max_v[0] + 1
                enc_oth = anchor1_sequeeze(LLL)
                enc_oth = img2patch(enc_oth, 1, 1, 1, 1, 1, 1)
                enc_oth = F.pad(enc_oth, paddings, "constant")
                context = concat_(anchor1_sequeeze(LLL_list_h[3]), anchor1_sequeeze(LLL_list_s[3])) 

            elif i == 2: # decompress LLL_2
                a = min_v[113]
                b = max_v[113] + 1
                enc_oth = anchor2_sequeeze(LLL)
                enc_oth = img2patch(enc_oth, 1, 1, 1, 1, 1, 1)
                enc_oth = F.pad(enc_oth, paddings, "constant")
                context = concat1_(anchor_LLL1, anchor2_sequeeze(LLL_list_h[3]), anchor2_sequeeze(LLL_list_s[3]))

            elif i == 3: # decompress LLL_3
                a = min_v[114]
                b = max_v[114] + 1
                enc_oth = anchor3_sequeeze(LLL)
                enc_oth = img2patch(enc_oth, 1, 1, 1, 1, 1, 1)
                enc_oth = F.pad(enc_oth, paddings, "constant")
                context = concat1_(concat_(anchor_LLL1, anchor_LLL2), anchor3_sequeeze(LLL_list_h[3]), anchor3_sequeeze(LLL_list_s[3]))

            elif i == 4: # decompress LLL_4
                a = min_v[115]
                b = max_v[115] + 1
                enc_oth = anchor4_sequeeze(LLL)
                enc_oth = img2patch(enc_oth, 1, 1, 1, 1, 1, 1)
                enc_oth = F.pad(enc_oth, paddings, "constant")
                context = concat1_(concat1_(anchor_LLL1,anchor_LLL2,anchor_LLL3), anchor4_sequeeze(LLL_list_h[3]), anchor4_sequeeze(LLL_list_s[3]))

            context = ref_padding(context)
            context = img2patch_padding(context, 5, 5, 5, 1, 1, 1, 2)
            TEST_time2 += time.time()-TEST_time_temp2
            
            cur_context = context
            TEST_time_temp2 = time.time()
            prob = models_dict['entropy_LLL_' + str(i)].inf_cdf_lower(cur_context, a, b)
            TEST_time3 += time.time()-TEST_time_temp2

            # convert to int and normalize
            TEST_time_temp2 = time.time()
            factor = FACTOR.to(prob.device)
            Lp = prob.shape[-1]
            new_max_value = factor + 1 - Lp
            add_tensor = torch.arange(Lp, dtype=torch.int32, device=prob.device).repeat(prob.shape[0], 1)
            prob = prob.mul(new_max_value).add(add_tensor).round().cpu().numpy().astype('int32')
            TEST_time4 += time.time()-TEST_time_temp2

            TEST_time_temp2 = time.time()
            index = dec.decode_batch(prob)
            TEST_time5 += time.time()-TEST_time_temp2
            

            enc_oth[:, 0, 0 + 2, 0 + 2, 0 + 2] = torch.from_numpy((np.array(index)+a).astype(np.float)).cuda()
            enc_oth = enc_oth[:, :, 2:-2, 2:-2, 2:-2]

            if i ==1:
                anchor_LLL1 = patch2img(enc_oth, anchor1_sequeeze(LLL).shape[2], anchor1_sequeeze(LLL).shape[3], anchor1_sequeeze(LLL).shape[4])
            elif i == 2:
                anchor_LLL2 = patch2img(enc_oth, anchor2_sequeeze(LLL).shape[2], anchor2_sequeeze(LLL).shape[3], anchor2_sequeeze(LLL).shape[4])
            elif i == 3:
                anchor_LLL3 = patch2img(enc_oth, anchor3_sequeeze(LLL).shape[2], anchor3_sequeeze(LLL).shape[3], anchor3_sequeeze(LLL).shape[4])
            elif i == 4:
                anchor_LLL4 = patch2img(enc_oth, anchor4_sequeeze(LLL).shape[2], anchor4_sequeeze(LLL).shape[3], anchor4_sequeeze(LLL).shape[4])
                # concat LLL
                LLL = de_anchor_unsequeeze1(anchor_LLL1) + de_anchor_unsequeeze2(anchor_LLL2) + de_anchor_unsequeeze3(anchor_LLL3) + de_anchor_unsequeeze4(anchor_LLL4)
            TEST_time1 += time.time()-TEST_time_temp1
            print('LLL_' + str(i) + ' decoded...')

        sub_list = ['HLL', 'LHL', 'HHL', 'LLH', 'HLH', 'LHH', 'HHH']

        for i in range(trans_steps):

            j = trans_steps - 1 - i

            if args.is_CT == 1:

                ##### CT

                if j == 3:
                    code_block_size_z = 1
                    code_block_size_x = 4
                    code_block_size_y = 2

                    stride_z = 1
                    stride_x = 4
                    stride_y = 2

                elif j == 2:
                    code_block_size_z = 2
                    code_block_size_x = 2
                    code_block_size_y = 4

                    stride_z = 2
                    stride_x = 2
                    stride_y = 4

                elif j == 1:
                    code_block_size_z = 4
                    code_block_size_x = 4
                    code_block_size_y = 8

                    stride_z = 4
                    stride_x = 4
                    stride_y = 8

                else:
                    code_block_size_z = 4
                    code_block_size_x = 16
                    code_block_size_y = 8

                    stride_z = 4
                    stride_x = 16
                    stride_y = 8

            else:

                #### MRI

                if j == 3:
                    code_block_size_z = 1
                    code_block_size_x = 1
                    code_block_size_y = 2

                    stride_z = 1
                    stride_x = 1
                    stride_y = 2

                elif j == 2:
                    code_block_size_z = 1
                    code_block_size_x = 7
                    code_block_size_y = 1

                    stride_z = 1
                    stride_x = 7
                    stride_y = 1

                elif j == 1:
                    code_block_size_z = 11
                    code_block_size_x = 7
                    code_block_size_y = 1

                    stride_z = 11
                    stride_x = 7
                    stride_y = 1

                else:
                    code_block_size_z = 11
                    code_block_size_x = 7
                    code_block_size_y = 2

                    stride_z = 11
                    stride_x = 7
                    stride_y = 2

            for i in range(1, 29):

                TEST_time_temp1 = time.time()
                TEST_time_temp2 = time.time()
                if i == 1: # decompress HLL_1
                    enc_oth = img2patch(anchor1_sequeeze(HLL_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), anchor1_sequeeze(HLL_list_h[j]), anchor1_sequeeze(HLL_list_s[j])), 1)

                elif i == 2: # decompress HLL_2
                    enc_oth = img2patch(anchor2_sequeeze(HLL_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), anchor_HLL1, anchor2_sequeeze(HLL_list_h[j]), anchor2_sequeeze(HLL_list_s[j])), 1)

                elif i == 3: # decompress HLL_3
                    enc_oth = img2patch(anchor3_sequeeze(HLL_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat_(anchor_HLL1,anchor_HLL2), anchor3_sequeeze(HLL_list_h[j]), anchor3_sequeeze(HLL_list_s[j])), 1)

                elif i == 4: # decompress HLL_4
                    enc_oth = img2patch(anchor4_sequeeze(HLL_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat1_(anchor_HLL1,anchor_HLL2,anchor_HLL3), anchor4_sequeeze(HLL_list_h[j]), anchor4_sequeeze(HLL_list_s[j])), 1)

                elif i == 5: # decompress LHL_1
                    enc_oth = img2patch(anchor1_sequeeze(LHL_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), anchor1_sequeeze(LHL_list_h[j]), anchor1_sequeeze(LHL_list_s[j])), 1)

                elif i == 6: # decompress LHL_2
                    enc_oth = img2patch(anchor2_sequeeze(LHL_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), anchor_LHL1, anchor2_sequeeze(LHL_list_h[j]), anchor2_sequeeze(LHL_list_s[j])), 1)

                elif i == 7: # decompress LHL_3
                    enc_oth = img2patch(anchor3_sequeeze(LHL_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat_(anchor_LHL1,anchor_LHL2), anchor3_sequeeze(LHL_list_h[j]), anchor3_sequeeze(LHL_list_s[j])), 1)

                elif i == 8: # decompress LHL_4
                    enc_oth = img2patch(anchor4_sequeeze(LHL_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat1_(anchor_LHL1,anchor_LHL2,anchor_LHL3), anchor4_sequeeze(LHL_list_h[j]), anchor4_sequeeze(LHL_list_s[j])), 1)

                elif i == 9: # decompress HHL_1
                    enc_oth = img2patch(anchor1_sequeeze(HHL_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), anchor1_sequeeze(HHL_list_h[j]), anchor1_sequeeze(HHL_list_s[j])), 1)

                elif i == 10: # decompress HHL_2
                    enc_oth = img2patch(anchor2_sequeeze(HHL_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), anchor_HHL1, anchor2_sequeeze(HHL_list_h[j]), anchor2_sequeeze(HHL_list_s[j])), 1)

                elif i == 11: # decompress HHL_3
                    enc_oth = img2patch(anchor3_sequeeze(HHL_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat_(anchor_HHL1, anchor_HHL2), anchor3_sequeeze(HHL_list_h[j]), anchor3_sequeeze(HHL_list_s[j])), 1)

                elif i == 12: # decompress HHL_4
                    enc_oth = img2patch(anchor4_sequeeze(HHL_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat1_(anchor_HHL1,anchor_HHL2,anchor_HHL3), anchor4_sequeeze(HHL_list_h[j]), anchor4_sequeeze(HHL_list_s[j])), 1)

                elif i == 13: # decompress LLH_1
                    enc_oth = img2patch(anchor1_sequeeze(LLH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]), anchor1_sequeeze(LLH_list_h[j]), anchor1_sequeeze(LLH_list_s[j])), 1)

                elif i == 14: # decompress LLH_2
                    enc_oth = img2patch(anchor2_sequeeze(LLH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL),concat3(HLL_list[j]),concat3(LHL_list[j]),concat3(HHL_list[j]),anchor_LLH1, anchor2_sequeeze(LLH_list_h[j]), anchor2_sequeeze(LLH_list_s[j])), 1)

                elif i == 15: # decompress LLH_3
                    enc_oth = img2patch(anchor3_sequeeze(LLH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]), concat_(anchor_LLH1,anchor_LLH2), anchor3_sequeeze(LLH_list_h[j]), anchor3_sequeeze(LLH_list_s[j])),1)

                elif i == 16: # decompress LLH_4
                    enc_oth = img2patch(anchor4_sequeeze(LLH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]), concat1_(anchor_LLH1,anchor_LLH2,anchor_LLH3), anchor4_sequeeze(LLH_list_h[j]), anchor4_sequeeze(LLH_list_s[j])),1)

                elif i == 17: # decompress HLH_1
                    enc_oth = img2patch(anchor1_sequeeze(HLH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat(
                (concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]), concat3(LLH_list[j]), anchor1_sequeeze(HLH_list_h[j]), anchor1_sequeeze(HLH_list_s[j])),1)

                elif i == 18: # decompress HLH_2
                    enc_oth = img2patch(anchor2_sequeeze(HLH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]),concat3(HHL_list[j]),concat3(LLH_list[j]), anchor_HLH1, anchor2_sequeeze(HLH_list_h[j]), anchor2_sequeeze(HLH_list_s[j])), 1)

                elif i == 19: # decompress HLH_3
                    enc_oth = img2patch(anchor3_sequeeze(HLH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL),concat3(HLL_list[j]),concat3(LHL_list[j]),concat3(HHL_list[j]),concat3(LLH_list[j]),concat_(anchor_HLH1,anchor_HLH2), anchor3_sequeeze(HLH_list_h[j]), anchor3_sequeeze(HLH_list_s[j])), 1)

                elif i == 20: # decompress HLH_4
                    enc_oth = img2patch(anchor4_sequeeze(HLH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]),concat3(LHL_list[j]),concat3(HHL_list[j]),concat3(LLH_list[j]),concat1_(anchor_HLH1,anchor_HLH2,anchor_HLH3), anchor4_sequeeze(HLH_list_h[j]), anchor4_sequeeze(HLH_list_s[j])), 1)

                elif i == 21: # decompress LHH_1
                    enc_oth = img2patch(anchor1_sequeeze(LHH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL),concat3(HLL_list[j]),concat3(LHL_list[j]),concat3(HHL_list[j]),concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]), anchor1_sequeeze(LHH_list_h[j]), anchor1_sequeeze(LHH_list_s[j])), 1)

                elif i == 22: # decompress LHH_2
                    enc_oth = img2patch(anchor2_sequeeze(LHH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL),concat3(HLL_list[j]),concat3(LHL_list[j]),concat3(HHL_list[j]),concat3(LLH_list[j]),concat3(HLH_list[j]),anchor_LHH1, anchor2_sequeeze(LHH_list_h[j]), anchor2_sequeeze(LHH_list_s[j])), 1)

                elif i == 23: # decompress LHH_3
                    enc_oth = img2patch(anchor3_sequeeze(LHH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL),concat3(HLL_list[j]),concat3(LHL_list[j]),concat3(HHL_list[j]),concat3(LLH_list[j]),concat3(HLH_list[j]),
                                                                                              concat_(anchor_LHH1,anchor_LHH2), anchor3_sequeeze(LHH_list_h[j]), anchor3_sequeeze(LHH_list_s[j])), 1)

                elif i == 24: # decompress LHH_4
                    enc_oth = img2patch(anchor4_sequeeze(LHH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL),concat3(HLL_list[j]),concat3(LHL_list[j]),concat3(HHL_list[j]),concat3(LLH_list[j]),concat3(HLH_list[j]),
                                                                                              concat1_(anchor_LHH1,anchor_LHH2,anchor_LHH3), anchor4_sequeeze(LHH_list_h[j]), anchor4_sequeeze(LHH_list_s[j])), 1)

                elif i == 25: # decompress HHH_1
                    enc_oth = img2patch(anchor1_sequeeze(HHH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL),concat3(HLL_list[j]),concat3(LHL_list[j]),concat3(HHL_list[j]),concat3(LLH_list[j]),concat3(HLH_list[j]),
                                                                                              concat3(LHH_list[j]), anchor1_sequeeze(HHH_list_h[j]), anchor1_sequeeze(HHH_list_s[j])), 1)
                    
                elif i == 26: # decompress HHH_2
                    enc_oth = img2patch(anchor2_sequeeze(HHH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),concat3(LLH_list[j]),
                                                                                              concat3(HLH_list[j]),concat3(LHH_list[j]),anchor_HHH1, anchor2_sequeeze(HHH_list_h[j]), anchor2_sequeeze(HHH_list_s[j])), 1)
    
                elif i == 27: # decompress HHH_3
                    enc_oth = img2patch(anchor3_sequeeze(HHH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL),concat3(HLL_list[j]),concat3(LHL_list[j]),concat3(HHL_list[j]),concat3(LLH_list[j]),concat3(HLH_list[j]),
                                                                                              concat3(LHH_list[j]),concat_(anchor_HHH1,anchor_HHH2), anchor3_sequeeze(HHH_list_h[j]), anchor3_sequeeze(HHH_list_s[j])), 1)
                
                elif i == 28: # decompress HHH_4
                    enc_oth = img2patch(anchor4_sequeeze(HHH_list[j]), code_block_size_z, code_block_size_x, code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL),concat3(HLL_list[j]),concat3(LHL_list[j]),concat3(HHL_list[j]),concat3(LLH_list[j]),concat3(HLH_list[j]),
                                                                                              concat3(LHH_list[j]),concat1_(anchor_HHH1,anchor_HHH2,anchor_HHH3), anchor4_sequeeze(HHH_list_h[j]), anchor4_sequeeze(HHH_list_s[j])), 1)
                
                context = ref_padding(context)
                context = img2patch_padding(context, code_block_size_z + 4, code_block_size_x + 4, code_block_size_y + 4, stride_z, stride_x, stride_y, 2)
                TEST_time2 += time.time()-TEST_time_temp2

                x = 28 * j + i
                a = min_v[x]
                b = max_v[x] + 1
                temp_add_tensor = 1
                for z_i in range(code_block_size_z):
                    for h_i in range(code_block_size_x):
                        for w_i in range(code_block_size_y):

                            cur_context = context[:, :, z_i:z_i + 5, h_i:h_i + 5, w_i:w_i + 5]
                            TEST_time_temp2 = time.time()
                            prob = models_dict['entropy_' + sub_list[(i-1)//4] + '_' + str((i-1)%4+1)].inf_cdf_lower(cur_context, a, b, j)
                            TEST_time3 += time.time()-TEST_time_temp2

                            # convert to int and normalize
                            TEST_time_temp2 = time.time()
                            if temp_add_tensor == 1:
                                temp_add_tensor = 0
                                factor = FACTOR.to(prob.device)
                                Lp = prob.shape[-1]
                                new_max_value = factor + 1 - Lp
                                add_tensor = torch.arange(Lp, dtype=torch.int32, device=prob.device).repeat(prob.shape[0], 1)
                            prob = prob.mul(new_max_value).add(add_tensor).round().cpu().numpy().astype('int32')
                            TEST_time4 += time.time()-TEST_time_temp2

                            TEST_time_temp2 = time.time()
                            index = dec.decode_batch(prob)
                            TEST_time5 += time.time()-TEST_time_temp2

                            enc_oth[:, 0, z_i + 2, h_i + 2, w_i + 2] = torch.from_numpy((np.array(index)+a).astype(np.float)).cuda()

                enc_oth = enc_oth[:, :, 2:-2, 2:-2, 2:-2]
                
                
                if i == 1:
                    anchor_HLL1 = patch2img(enc_oth, anchor1_sequeeze(HLL_list[j]).shape[2], anchor1_sequeeze(HLL_list[j]).shape[3], anchor1_sequeeze(HLL_list[j]).shape[4])
                    
                elif i == 2:
                    anchor_HLL2 = patch2img(enc_oth, anchor2_sequeeze(HLL_list[j]).shape[2], anchor2_sequeeze(HLL_list[j]).shape[3], anchor2_sequeeze(HLL_list[j]).shape[4])

                elif i == 3:
                    anchor_HLL3 = patch2img(enc_oth, anchor3_sequeeze(HLL_list[j]).shape[2], anchor3_sequeeze(HLL_list[j]).shape[3], anchor3_sequeeze(HLL_list[j]).shape[4])

                elif i == 4:
                    anchor_HLL4 = patch2img(enc_oth, anchor4_sequeeze(HLL_list[j]).shape[2], anchor4_sequeeze(HLL_list[j]).shape[3], anchor4_sequeeze(HLL_list[j]).shape[4])
                    # concat HLL
                    HLL_list[j] = de_anchor_unsequeeze1(anchor_HLL1) + de_anchor_unsequeeze2(anchor_HLL2) + de_anchor_unsequeeze3(anchor_HLL3) + de_anchor_unsequeeze4(anchor_HLL4)

                elif i == 5:
                    anchor_LHL1 = patch2img(enc_oth, anchor1_sequeeze(LHL_list[j]).shape[2], anchor1_sequeeze(LHL_list[j]).shape[3], anchor1_sequeeze(LHL_list[j]).shape[4])

                elif i == 6:
                    anchor_LHL2 = patch2img(enc_oth, anchor2_sequeeze(LHL_list[j]).shape[2], anchor2_sequeeze(LHL_list[j]).shape[3], anchor2_sequeeze(LHL_list[j]).shape[4])

                elif i == 7:
                    anchor_LHL3 = patch2img(enc_oth, anchor3_sequeeze(LHL_list[j]).shape[2], anchor3_sequeeze(LHL_list[j]).shape[3], anchor3_sequeeze(LHL_list[j]).shape[4])

                elif i == 8:
                    anchor_LHL4 = patch2img(enc_oth, anchor4_sequeeze(LHL_list[j]).shape[2], anchor4_sequeeze(LHL_list[j]).shape[3], anchor4_sequeeze(LHL_list[j]).shape[4])
                    # concat LHL
                    LHL_list[j] = de_anchor_unsequeeze1(anchor_LHL1) + de_anchor_unsequeeze2(anchor_LHL2) + de_anchor_unsequeeze3(anchor_LHL3) + de_anchor_unsequeeze4(anchor_LHL4)
                    
                elif i == 9:
                    anchor_HHL1 = patch2img(enc_oth, anchor1_sequeeze(HHL_list[j]).shape[2], anchor1_sequeeze(HHL_list[j]).shape[3], anchor1_sequeeze(HHL_list[j]).shape[4])

                elif i == 10:
                    anchor_HHL2 = patch2img(enc_oth, anchor2_sequeeze(HHL_list[j]).shape[2], anchor2_sequeeze(HHL_list[j]).shape[3], anchor2_sequeeze(HHL_list[j]).shape[4])

                elif i == 11:
                    anchor_HHL3 = patch2img(enc_oth, anchor3_sequeeze(HHL_list[j]).shape[2], anchor3_sequeeze(HHL_list[j]).shape[3], anchor3_sequeeze(HHL_list[j]).shape[4])

                elif i == 12:
                    anchor_HHL4 = patch2img(enc_oth, anchor4_sequeeze(HHL_list[j]).shape[2], anchor4_sequeeze(HHL_list[j]).shape[3], anchor4_sequeeze(HHL_list[j]).shape[4])
                    # concat HHL
                    HHL_list[j] = de_anchor_unsequeeze1(anchor_HHL1) + de_anchor_unsequeeze2(anchor_HHL2) + de_anchor_unsequeeze3(anchor_HHL3) + de_anchor_unsequeeze4(anchor_HHL4)
                    
                elif i == 13:
                    anchor_LLH1 = patch2img(enc_oth, anchor1_sequeeze(LLH_list[j]).shape[2], anchor1_sequeeze(LLH_list[j]).shape[3], anchor1_sequeeze(LLH_list[j]).shape[4])

                elif i == 14:
                    anchor_LLH2 = patch2img(enc_oth, anchor2_sequeeze(LLH_list[j]).shape[2], anchor2_sequeeze(LLH_list[j]).shape[3], anchor2_sequeeze(LLH_list[j]).shape[4])

                elif i == 15:
                    anchor_LLH3 = patch2img(enc_oth, anchor3_sequeeze(LLH_list[j]).shape[2], anchor3_sequeeze(LLH_list[j]).shape[3], anchor3_sequeeze(LLH_list[j]).shape[4])

                elif i == 16:
                    anchor_LLH4 = patch2img(enc_oth, anchor4_sequeeze(LLH_list[j]).shape[2], anchor4_sequeeze(LLH_list[j]).shape[3], anchor4_sequeeze(LLH_list[j]).shape[4])
                    # concat LLH
                    LLH_list[j] = de_anchor_unsequeeze1(anchor_LLH1) + de_anchor_unsequeeze2(anchor_LLH2) + de_anchor_unsequeeze3(anchor_LLH3) + de_anchor_unsequeeze4(anchor_LLH4)

                elif i == 17:
                    anchor_HLH1 = patch2img(enc_oth, anchor1_sequeeze(HLH_list[j]).shape[2], anchor1_sequeeze(HLH_list[j]).shape[3], anchor1_sequeeze(HLH_list[j]).shape[4])

                elif i == 18:
                    anchor_HLH2 = patch2img(enc_oth, anchor2_sequeeze(HLH_list[j]).shape[2], anchor2_sequeeze(HLH_list[j]).shape[3], anchor2_sequeeze(HLH_list[j]).shape[4])

                elif i == 19:
                    anchor_HLH3 = patch2img(enc_oth, anchor3_sequeeze(HLH_list[j]).shape[2], anchor3_sequeeze(HLH_list[j]).shape[3], anchor3_sequeeze(HLH_list[j]).shape[4])

                elif i == 20:
                    anchor_HLH4 = patch2img(enc_oth, anchor4_sequeeze(HLH_list[j]).shape[2], anchor4_sequeeze(HLH_list[j]).shape[3], anchor4_sequeeze(HLH_list[j]).shape[4])
                    # concat HLH
                    HLH_list[j] = de_anchor_unsequeeze1(anchor_HLH1) + de_anchor_unsequeeze2(anchor_HLH2) + de_anchor_unsequeeze3(anchor_HLH3) + de_anchor_unsequeeze4(anchor_HLH4)
                    
                elif i == 21:
                    anchor_LHH1 = patch2img(enc_oth, anchor1_sequeeze(LHH_list[j]).shape[2], anchor1_sequeeze(LHH_list[j]).shape[3], anchor1_sequeeze(LHH_list[j]).shape[4])

                elif i == 22:
                    anchor_LHH2 = patch2img(enc_oth, anchor2_sequeeze(LHH_list[j]).shape[2], anchor2_sequeeze(LHH_list[j]).shape[3], anchor2_sequeeze(LHH_list[j]).shape[4])

                elif i == 23:
                    anchor_LHH3 = patch2img(enc_oth, anchor3_sequeeze(LHH_list[j]).shape[2], anchor3_sequeeze(LHH_list[j]).shape[3], anchor3_sequeeze(LHH_list[j]).shape[4])

                elif i == 24:
                    anchor_LHH4 = patch2img(enc_oth, anchor4_sequeeze(LHH_list[j]).shape[2], anchor4_sequeeze(LHH_list[j]).shape[3], anchor4_sequeeze(LHH_list[j]).shape[4])
                    # concat LHH
                    LHH_list[j] = de_anchor_unsequeeze1(anchor_LHH1) + de_anchor_unsequeeze2(anchor_LHH2) + de_anchor_unsequeeze3(anchor_LHH3) + de_anchor_unsequeeze4(anchor_LHH4)

                elif i == 25:
                    anchor_HHH1 = patch2img(enc_oth, anchor1_sequeeze(HHH_list[j]).shape[2], anchor1_sequeeze(HHH_list[j]).shape[3], anchor1_sequeeze(HHH_list[j]).shape[4])

                elif i == 26:
                    anchor_HHH2 = patch2img(enc_oth, anchor2_sequeeze(HHH_list[j]).shape[2], anchor2_sequeeze(HHH_list[j]).shape[3], anchor2_sequeeze(HHH_list[j]).shape[4])

                elif i == 27:
                    anchor_HHH3 = patch2img(enc_oth, anchor3_sequeeze(HHH_list[j]).shape[2], anchor3_sequeeze(HHH_list[j]).shape[3], anchor3_sequeeze(HHH_list[j]).shape[4])

                elif i == 28:
                    anchor_HHH4 = patch2img(enc_oth, anchor4_sequeeze(HHH_list[j]).shape[2], anchor4_sequeeze(HHH_list[j]).shape[3], anchor4_sequeeze(HHH_list[j]).shape[4])
                    # concat HHH
                    HHH_list[j] = de_anchor_unsequeeze1(anchor_HHH1) + de_anchor_unsequeeze2(anchor_HHH2) + de_anchor_unsequeeze3(anchor_HHH3) + de_anchor_unsequeeze4(anchor_HHH4)
                TEST_time1 += time.time()-TEST_time_temp1
                print(sub_list[(i-1)//4] + '_' + str((i-1)%4+1) + str(j) + ' decoded...')

            HLL_list[j] = Recon(HLL_list[j].type(torch.int32), HLL_list_h[j], HLL_list_s[j])
            LHL_list[j] = Recon(LHL_list[j].type(torch.int32), LHL_list_h[j], LHL_list_s[j])
            HHL_list[j] = Recon(HHL_list[j].type(torch.int32), HHL_list_h[j], HHL_list_s[j])
            LLH_list[j] = Recon(LLH_list[j].type(torch.int32), LLH_list_h[j], LLH_list_s[j])
            HLH_list[j] = Recon(HLH_list[j].type(torch.int32), HLH_list_h[j], HLH_list_s[j])
            LHH_list[j] = Recon(LHH_list[j].type(torch.int32), LHH_list_h[j], LHH_list_s[j])
            HHH_list[j] = Recon(HHH_list[j].type(torch.int32), HHH_list_h[j], HHH_list_s[j])

            LLL = Recon(LLL.type(torch.int32), LLL_list_h[j], LLL_list_s[j])
            LLL = models_dict['transform'].inverse_trans(LLL, HLL_list[j], LHL_list[j], HHL_list[j], LLH_list[j], HLH_list[j], LHH_list[j], HHH_list[j])
            LLL = LLL.type(torch.int32)

        recon = LLL[0, 0, 0:depth, 0:height, 0:width]
        recon = recon.cpu().data.numpy().astype(np.float32)
        save_nii(recon, args.recon_dir + '/' + 'recon_' + bin_name + '.nii')
        
        print(TEST_time1, TEST_time2, TEST_time3, TEST_time4, TEST_time5)
        
        logfile.flush()
