import argparse
import json

from enc_lossless_light_mri import enc_lossless
# from enc_lossless_light_ct import enc_lossless
from enc_lossy_ct import enc_lossy
# from enc_lossy_mri import enc_lossy

parser = argparse.ArgumentParser(description='Biomedical image coding CFE', conflict_handler='resolve')
# parameters
parser.add_argument('--cfg_file', type=str, default='/code/aiWave_rans/cfg/encode_lossy_ct.cfg')
# parser.add_argument('--cfg_file', type=str, default='/code/aiWave_rans/cfg/encode_lossy_mri.cfg')

args, unknown = parser.parse_known_args()

cfg_file = args.cfg_file
with open(cfg_file, 'r') as f:
    cfg_dict = json.load(f)
    
    for key, value in cfg_dict.items():
        if isinstance(value, int):
            parser.add_argument('--{}'.format(key), type=int, default=value)
        elif isinstance(value, float):
            parser.add_argument('--{}'.format(key), type=float, default=value)
        else:
            parser.add_argument('--{}'.format(key), type=str, default=value)

cfg_args, unknown = parser.parse_known_args()

parser.add_argument('--input_dir', type=str, default=cfg_args.input_dir)
# parser.add_argument('--input_dir_high', type=str, default=cfg_args.input_dir_high)
parser.add_argument('--img_name', type=str, default=cfg_args.img_name)
parser.add_argument('--bin_dir', type=str, default=cfg_args.bin_dir)
parser.add_argument('--log_dir', type=str, default=cfg_args.log_dir)
parser.add_argument('--recon_dir', type=str, default=cfg_args.recon_dir)
parser.add_argument('--isLossless', type=int, default=cfg_args.isLossless)
parser.add_argument('--init_scale', type=int, default=cfg_args.init_scale)
parser.add_argument('--is_CT', type=int, default=cfg_args.is_CT)
parser.add_argument('--model_path', type=str, default=cfg_args.model_path) # store all models


def main():
    args = parser.parse_args()

    if args.isLossless == 0:
        enc_lossy(args)
    else:
        enc_lossless(args)

if __name__ == "__main__":
    main()
