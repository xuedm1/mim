import Model
import argparse
import torch
from torch.autograd import Variable
import os
import glob as gb
from PIL import Image
import numpy as np
from torch.nn import functional as F
import Quant
import copy
import time


model_qps = range(28) # 13 MSE models + 14 perceptual models + 1 lossless model
model_lambdas = [0.4, 0.25, 0.16, 0.10, 0.0625, 0.039, 0.024, 0.015, 0.0095, 0.006, 0.0037, 0.002, 0.0012,
                 0.4, 0.25, 0.16, 0.10, 0.018, 0.012, 0.0075, 0.0048, 0.0032, 0.002, 0.00145, 0.0008, 0.00055, 0.00035,
                 9999
                 ]
qp_shifts=[
[8, 7.5, 8.5, 7, 9, 6.5, 9.5],
[8, 7.5, 8.5, 7, 9, 6.5, 9.5],
[8, 7.5, 8.5, 7, 9, 6.5, 9.5],
[16, 15, 17, 14, 18, 13, 19],
[16, 15, 17, 14, 18, 13, 19],
[16, 15, 18, 14, 20, 13, 22],
[32, 30, 33, 34, 35, 36, 37],
[32, 31, 34, 30, 36, 29, 38],
[32, 30, 36, 28, 40, 26, 44],
[64, 60, 66, 56, 68, 70, 72],
[64, 62, 70, 58, 76, 54, 82],
[64, 58, 72, 52, 80, 46, 88],
[64, 56, 72, 48, 80, 40, 88],

[8, 7.5, 8.5, 7, 9, 6.5, 9.5],
[8, 7.5, 8.5, 7, 9, 6.5, 9.5],
[8, 7.5, 8.5, 7, 9, 6.5, 9.5],
[16, 15, 17, 14, 18, 13, 19],
[16, 15, 17, 14, 18, 13, 19],
[16, 15, 18, 14, 20, 13, 22],
[32, 30, 33, 34, 35, 36, 37],
[32, 31, 34, 30, 36, 29, 38],
[32, 30, 36, 28, 40, 26, 44],
[64, 60, 66, 56, 68, 52, 70],
[64, 62, 70, 60, 76, 58, 82],
[64, 58, 72, 52, 80, 46, 88],
[64, 56, 72, 48, 80, 40, 88],
[64, 56, 72, 48, 80, 40, 88],

[1]
]


def img2patch(x, z, h, w, stride_z, stride_x, stride_y):
    size0, size1, size2, size3, size4 = x.size()
    x_tmp = x[:, :, 0:z, 0:h, 0:w]
    for i in range(0, size2, stride_z):
        for j in range(0, size3, stride_x):
            for k in range(0, size4, stride_y):
                x_tmp = torch.cat((x_tmp, x[:, :, i:i+z, j:j+h, k:k+w]), dim=0)
    return x_tmp[size0::, :, :, :, :]



def patch2img(x, img_z, img_h, img_w):
    size0, size1, size2, size3, size4 = x.size()
    img = torch.zeros((1, 1, img_z, img_h, img_w), device='cuda')
    c = 0

    i_max = img_z // size2
    j_max = img_h // size3
    k_max = img_w // size4

    isize2 = 0
    for i in range(i_max):
        jsize3 = 0
        for j in range(j_max):
            ksize4 = 0
            for k in range(k_max):
                img[:, :, isize2:isize2+size2, jsize3:jsize3+size3, ksize4:ksize4+size4] = x[c:c+1, :, :, :, :]
                c = c + 1
                ksize4 = ksize4 + size4
            jsize3 = jsize3 + size3
        isize2 = isize2 + size2
    return img


def img2patch_padding(x, z, h, w, stride_z, stride_x,stride_y,padding):
    size0, size1, size2, size3, size4 = x.size()
    x_tmp = x[:, :, 0:z, 0:h, 0:w]
    i_max = size2 - 2 * padding
    j_max = size3 - 2 * padding
    k_max = size4 - 2 * padding
    for i in range(0, i_max, stride_z):
        for j in range(0, j_max, stride_x):
            for k in range(0, k_max, stride_y):
                x_tmp = torch.cat((x_tmp, x[:, :, i:i+z, j:j+h, k:k+w]), dim=0)
    return x_tmp[size0::, :, :, :, :]


def img2patch_padding_ct(x, z, h, w, stride_z, stride_x,stride_y,padding_z, padding_x, padding_y):
    size0, size1, size2, size3, size4 = x.size()
    x_tmp = x[:, :, 0:z, 0:h, 0:w]
    i_max = size2 - 2 * padding_z
    j_max = size3 - 2 * padding_x
    k_max = size4 - 2 * padding_y
    for i in range(0, i_max, stride_z):
        for j in range(0, j_max, stride_x):
            for k in range(0, k_max, stride_y):
                x_tmp = torch.cat((x_tmp, x[:, :, i:i+z, j:j+h, k:k+w]), dim=0)
    return x_tmp[size0::, :, :, :, :]


def rgb2yuv_lossless(x):
    x = np.array(x, dtype=np.int32)

    r = x[:, :, 0:1]
    g = x[:, :, 1:2]
    b = x[:, :, 2:3]

    yuv = np.zeros_like(x, dtype=np.int32)

    Co = r - b
    tmp = b + np.right_shift(Co, 1)
    Cg = g - tmp
    Y = tmp + np.right_shift(Cg, 1)

    yuv[:, :, 0:1] = Y
    yuv[:, :, 1:2] = Co
    yuv[:, :, 2:3] = Cg

    return yuv


def yuv2rgb_lossless(x):
    x = np.array(x, dtype=np.int32)

    Y = x[:, :, 0:1]
    Co = x[:, :, 1:2]
    Cg = x[:, :, 2:3]

    rgb = np.zeros_like(x, dtype=np.int32)

    tmp = Y - np.right_shift(Cg, 1)
    g = Cg + tmp
    b = tmp - np.right_shift(Co, 1)
    r = b + Co

    rgb[:, :, 0:1] = r
    rgb[:, :, 1:2] = g
    rgb[:, :, 2:3] = b

    return rgb


def rgb2yuv(x):
    convert_mat = np.array([[0.299, 0.587, 0.114],
                            [-0.169, -0.331, 0.499],
                            [0.499, -0.418, -0.0813]], dtype=np.float32)

    y = x[:, 0:1, :, :] * convert_mat[0, 0] +\
        x[:, 1:2, :, :] * convert_mat[0, 1] +\
        x[:, 2:3, :, :] * convert_mat[0, 2]

    u = x[:, 0:1, :, :] * convert_mat[1, 0] +\
        x[:, 1:2, :, :] * convert_mat[1, 1] +\
        x[:, 2:3, :, :] * convert_mat[1, 2] + 128.

    v = x[:, 0:1, :, :] * convert_mat[2, 0] +\
        x[:, 1:2, :, :] * convert_mat[2, 1] +\
        x[:, 2:3, :, :] * convert_mat[2, 2] + 128.
    return torch.cat((y, u, v), dim=1)


def yuv2rgb(x):
    inverse_convert_mat = np.array([[1.0, 0.0, 1.402],
                                    [1.0, -0.344, -0.714],
                                    [1.0, 1.772, 0.0]], dtype=np.float32)
    r = x[:, 0:1, :, :] * inverse_convert_mat[0, 0] +\
        (x[:, 1:2, :, :] - 128.) * inverse_convert_mat[0, 1] +\
        (x[:, 2:3, :, :] - 128.) * inverse_convert_mat[0, 2]
    g = x[:, 0:1, :, :] * inverse_convert_mat[1, 0] +\
        (x[:, 1:2, :, :] - 128.) * inverse_convert_mat[1, 1] +\
        (x[:, 2:3, :, :] - 128.) * inverse_convert_mat[1, 2]
    b = x[:, 0:1, :, :] * inverse_convert_mat[2, 0] +\
        (x[:, 1:2, :, :] - 128.) * inverse_convert_mat[2, 1] +\
        (x[:, 2:3, :, :] - 128.) * inverse_convert_mat[2, 2]
    return torch.cat((r, g, b), dim=1)


def anchor_sequeeze(y):

    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H, W // 2], device=y.device)
    anchor[:, :, 0::2, 1::2, :] = y[:, :, 0::2, 1::2, 0::2]
    anchor[:, :, 0::2, 0::2, :] = y[:, :, 0::2, 0::2, 1::2]
    anchor[:, :, 1::2, 1::2, :] = y[:, :, 1::2, 1::2, 1::2]
    anchor[:, :, 1::2, 0::2, :] = y[:, :, 1::2, 0::2, 0::2]

    return anchor


def nonanchor_sequeeze(y):

    B, C, Z, H, W = y.shape
    nonanchor = torch.zeros([B, C, Z, H, W // 2], device=y.device)
    nonanchor[:, :, 0::2, 1::2, :] = y[:, :, 0::2, 1::2, 1::2]
    nonanchor[:, :, 0::2, 0::2, :] = y[:, :, 0::2, 0::2, 0::2]
    nonanchor[:, :, 1::2, 1::2, :] = y[:, :, 1::2, 1::2, 0::2]
    nonanchor[:, :, 1::2, 0::2, :] = y[:, :, 1::2, 0::2, 1::2]

    return nonanchor


def find_min_and_max(LLL, HLL_list, LHL_list, HHL_list, LLH_list, HLH_list, LHH_list, HHH_list):

    min_v = [100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.]
    max_v = [-100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.]


    tmp = LLL[0, 0, :, :, :]
    min_tmp = torch.min(tmp).item()
    max_tmp = torch.max(tmp).item()
    if min_tmp < min_v[0]:
        min_v[0] = min_tmp
    if max_tmp > max_v[0]:
        max_v[0] = max_tmp

    for s_j in range(4):
        s_i = 4 - 1 - s_j
        tmp = HLL_list[s_i][0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[7 * s_i + 1]:
            min_v[7 * s_i + 1] = min_tmp
        if max_tmp > max_v[7 * s_i + 1]:
            max_v[7 * s_i + 1] = max_tmp

        tmp = LHL_list[s_i][0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[7 * s_i + 2]:
            min_v[7 * s_i + 2] = min_tmp
        if max_tmp > max_v[7 * s_i + 2]:
            max_v[7 * s_i + 2] = max_tmp

        tmp = HHL_list[s_i][0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[7 * s_i + 3]:
            min_v[7 * s_i + 3] = min_tmp
        if max_tmp > max_v[7 * s_i + 3]:
            max_v[7 * s_i + 3] = max_tmp

        tmp = LLH_list[s_i][0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[7 * s_i + 4]:
            min_v[7 * s_i + 4] = min_tmp
        if max_tmp > max_v[7 * s_i + 4]:
            max_v[7 * s_i + 4] = max_tmp

        tmp = HLH_list[s_i][0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[7 * s_i + 5]:
            min_v[7 * s_i + 5] = min_tmp
        if max_tmp > max_v[7 * s_i + 5]:
            max_v[7 * s_i + 5] = max_tmp

        tmp = LHH_list[s_i][0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[7 * s_i + 6]:
            min_v[7 * s_i + 6] = min_tmp
        if max_tmp > max_v[7 * s_i + 6]:
            max_v[7 * s_i + 6] = max_tmp

        tmp = HHH_list[s_i][0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[7 * s_i + 7]:
            min_v[7 * s_i + 7] = min_tmp
        if max_tmp > max_v[7 * s_i + 7]:
            max_v[7 * s_i + 7] = max_tmp
    min_v = (np.array(min_v)).astype(np.int32)
    max_v = (np.array(max_v)).astype(np.int32)
    return min_v, max_v


def find_min_and_max_2(LLL, HLL_list, LHL_list, HHL_list, LLH_list, HLH_list, LHH_list, HHH_list):

    min_v = [100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000.]
    max_v = [-100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000.]

    tmp = anchor_sequeeze(LLL)
    tmp = tmp[0, 0, :, :, :]
    min_tmp = torch.min(tmp).item()
    max_tmp = torch.max(tmp).item()
    if min_tmp < min_v[0]:
        min_v[0] = min_tmp
    if max_tmp > max_v[0]:
        max_v[0] = max_tmp

    tmp = nonanchor_sequeeze(LLL)
    tmp = tmp[0, 0, :, :, :]
    min_tmp = torch.min(tmp).item()
    max_tmp = torch.max(tmp).item()
    if min_tmp < min_v[57]:
        min_v[57] = min_tmp
    if max_tmp > max_v[57]:
        max_v[57] = max_tmp

    for s_j in range(4):

        s_i = 4 - 1 - s_j

        tmp = anchor_sequeeze(HLL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[14 * s_i + 1]:
            min_v[14 * s_i + 1] = min_tmp
        if max_tmp > max_v[14 * s_i + 1]:
            max_v[14 * s_i + 1] = max_tmp

        tmp = nonanchor_sequeeze(HLL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[14 * s_i + 2]:
            min_v[14 * s_i + 2] = min_tmp
        if max_tmp > max_v[14 * s_i + 2]:
            max_v[14 * s_i + 2] = max_tmp

        tmp = anchor_sequeeze(LHL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[14 * s_i + 3]:
            min_v[14 * s_i + 3] = min_tmp
        if max_tmp > max_v[14 * s_i + 3]:
            max_v[14 * s_i + 3] = max_tmp

        tmp = nonanchor_sequeeze(LHL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[14 * s_i + 4]:
            min_v[14 * s_i + 4] = min_tmp
        if max_tmp > max_v[14 * s_i + 4]:
            max_v[14 * s_i + 4] = max_tmp

        tmp = anchor_sequeeze(HHL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[14 * s_i + 5]:
            min_v[14 * s_i + 5] = min_tmp
        if max_tmp > max_v[14 * s_i + 5]:
            max_v[14 * s_i + 5] = max_tmp

        tmp = nonanchor_sequeeze(HHL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[14 * s_i + 6]:
            min_v[14 * s_i + 6] = min_tmp
        if max_tmp > max_v[14 * s_i + 6]:
            max_v[14 * s_i + 6] = max_tmp

        tmp = anchor_sequeeze(LLH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[14 * s_i + 7]:
            min_v[14 * s_i + 7] = min_tmp
        if max_tmp > max_v[14 * s_i + 7]:
            max_v[14 * s_i + 7] = max_tmp

        tmp = nonanchor_sequeeze(LLH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[14 * s_i + 8]:
            min_v[14 * s_i + 8] = min_tmp
        if max_tmp > max_v[14 * s_i + 8]:
            max_v[14 * s_i + 8] = max_tmp

        tmp = anchor_sequeeze(HLH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[14 * s_i + 9]:
            min_v[14 * s_i + 9] = min_tmp
        if max_tmp > max_v[14 * s_i + 9]:
            max_v[14 * s_i + 9] = max_tmp

        tmp = nonanchor_sequeeze(HLH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[14 * s_i + 10]:
            min_v[14 * s_i + 10] = min_tmp
        if max_tmp > max_v[14 * s_i + 10]:
            max_v[14 * s_i + 10] = max_tmp

        tmp = anchor_sequeeze(LHH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[14 * s_i + 11]:
            min_v[14 * s_i + 11] = min_tmp
        if max_tmp > max_v[14 * s_i + 11]:
            max_v[14 * s_i + 11] = max_tmp

        tmp = nonanchor_sequeeze(LHH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[14 * s_i + 12]:
            min_v[14 * s_i + 12] = min_tmp
        if max_tmp > max_v[14 * s_i + 12]:
            max_v[14 * s_i + 12] = max_tmp

        tmp = anchor_sequeeze(HHH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[14 * s_i + 13]:
            min_v[14 * s_i + 13] = min_tmp
        if max_tmp > max_v[14 * s_i + 13]:
            max_v[14 * s_i + 13] = max_tmp

        tmp = nonanchor_sequeeze(HHH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[14 * s_i + 14]:
            min_v[14 * s_i + 14] = min_tmp
        if max_tmp > max_v[14 * s_i + 14]:
            max_v[14 * s_i + 14] = max_tmp

    min_v = (np.array(min_v)).astype(np.int32)
    max_v = (np.array(max_v)).astype(np.int32)
    return min_v, max_v


def anchor1_sequeeze(y):

    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H//2, W//2], device=y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 0::2, 0::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 1::2, 1::2]

    return anchor

def anchor2_sequeeze(y):

    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H//2, W//2], device=y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 1::2, 1::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 0::2, 0::2]

    return anchor

def anchor3_sequeeze(y):

    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H//2, W//2], device=y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 0::2, 1::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 1::2, 0::2]

    return anchor

def anchor4_sequeeze(y):

    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H//2, W//2], device=y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 1::2, 0::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 0::2, 1::2]

    return anchor


def find_min_and_max_4(LLL, HLL_list, LHL_list, HHL_list, LLH_list, HLH_list, LHH_list, HHH_list):

    min_v = [100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.,
             100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000., 100000000.]
    max_v = [-100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000., -100000000.,
             -100000000., -100000000., -100000000., -100000000.]

    tmp = anchor1_sequeeze(LLL)
    tmp = tmp[0, 0, :, :, :]
    min_tmp = torch.min(tmp).item()
    max_tmp = torch.max(tmp).item()
    if min_tmp < min_v[0]:
        min_v[0] = min_tmp
    if max_tmp > max_v[0]:
        max_v[0] = max_tmp

    tmp = anchor2_sequeeze(LLL)
    tmp = tmp[0, 0, :, :, :]
    min_tmp = torch.min(tmp).item()
    max_tmp = torch.max(tmp).item()
    if min_tmp < min_v[113]:
        min_v[113] = min_tmp
    if max_tmp > max_v[113]:
        max_v[113] = max_tmp

    tmp = anchor3_sequeeze(LLL)
    tmp = tmp[0, 0, :, :, :]
    min_tmp = torch.min(tmp).item()
    max_tmp = torch.max(tmp).item()
    if min_tmp < min_v[114]:
        min_v[114] = min_tmp
    if max_tmp > max_v[114]:
        max_v[114] = max_tmp

    tmp = anchor4_sequeeze(LLL)
    tmp = tmp[0, 0, :, :, :]
    min_tmp = torch.min(tmp).item()
    max_tmp = torch.max(tmp).item()
    if min_tmp < min_v[115]:
        min_v[115] = min_tmp
    if max_tmp > max_v[115]:
        max_v[115] = max_tmp

    for s_j in range(4):

        s_i = 4 - 1 - s_j

        tmp = anchor1_sequeeze(HLL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 1]:
            min_v[28 * s_i + 1] = min_tmp
        if max_tmp > max_v[28 * s_i + 1]:
            max_v[28 * s_i + 1] = max_tmp

        tmp = anchor2_sequeeze(HLL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 2]:
            min_v[28 * s_i + 2] = min_tmp
        if max_tmp > max_v[28 * s_i + 2]:
            max_v[28 * s_i + 2] = max_tmp

        tmp = anchor3_sequeeze(HLL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 3]:
            min_v[28 * s_i + 3] = min_tmp
        if max_tmp > max_v[28 * s_i + 3]:
            max_v[28 * s_i + 3] = max_tmp

        tmp = anchor4_sequeeze(HLL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 4]:
            min_v[28 * s_i + 4] = min_tmp
        if max_tmp > max_v[28 * s_i + 4]:
            max_v[28 * s_i + 4] = max_tmp

        tmp = anchor1_sequeeze(LHL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 5]:
            min_v[28 * s_i + 5] = min_tmp
        if max_tmp > max_v[28 * s_i + 5]:
            max_v[28 * s_i + 5] = max_tmp

        tmp = anchor2_sequeeze(LHL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 6]:
            min_v[28 * s_i + 6] = min_tmp
        if max_tmp > max_v[28 * s_i + 6]:
            max_v[28 * s_i + 6] = max_tmp

        tmp = anchor3_sequeeze(LHL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 7]:
            min_v[28 * s_i + 7] = min_tmp
        if max_tmp > max_v[28 * s_i + 7]:
            max_v[28 * s_i + 7] = max_tmp

        tmp = anchor4_sequeeze(LHL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 8]:
            min_v[28 * s_i + 8] = min_tmp
        if max_tmp > max_v[28 * s_i + 8]:
            max_v[28 * s_i + 8] = max_tmp

        tmp = anchor1_sequeeze(HHL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 9]:
            min_v[28 * s_i + 9] = min_tmp
        if max_tmp > max_v[28 * s_i + 9]:
            max_v[28 * s_i + 9] = max_tmp

        tmp = anchor2_sequeeze(HHL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 10]:
            min_v[28 * s_i + 10] = min_tmp
        if max_tmp > max_v[28 * s_i + 10]:
            max_v[28 * s_i + 10] = max_tmp

        tmp = anchor3_sequeeze(HHL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 11]:
            min_v[28 * s_i + 11] = min_tmp
        if max_tmp > max_v[28 * s_i + 11]:
            max_v[28 * s_i + 11] = max_tmp

        tmp = anchor4_sequeeze(HHL_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 12]:
            min_v[28 * s_i + 12] = min_tmp
        if max_tmp > max_v[28 * s_i + 12]:
            max_v[28 * s_i + 12] = max_tmp

        tmp = anchor1_sequeeze(LLH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 13]:
            min_v[28 * s_i + 13] = min_tmp
        if max_tmp > max_v[28 * s_i + 13]:
            max_v[28 * s_i + 13] = max_tmp

        tmp = anchor2_sequeeze(LLH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 14]:
            min_v[28 * s_i + 14] = min_tmp
        if max_tmp > max_v[28 * s_i + 14]:
            max_v[28 * s_i + 14] = max_tmp

        tmp = anchor3_sequeeze(LLH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 15]:
            min_v[28 * s_i + 15] = min_tmp
        if max_tmp > max_v[28 * s_i + 15]:
            max_v[28 * s_i + 15] = max_tmp

        tmp = anchor4_sequeeze(LLH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 16]:
            min_v[28 * s_i + 16] = min_tmp
        if max_tmp > max_v[28 * s_i + 16]:
            max_v[28 * s_i + 16] = max_tmp

        tmp = anchor1_sequeeze(HLH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 17]:
            min_v[28 * s_i + 17] = min_tmp
        if max_tmp > max_v[28 * s_i + 17]:
            max_v[28 * s_i + 17] = max_tmp

        tmp = anchor2_sequeeze(HLH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 18]:
            min_v[28 * s_i + 18] = min_tmp
        if max_tmp > max_v[28 * s_i + 18]:
            max_v[28 * s_i + 18] = max_tmp

        tmp = anchor3_sequeeze(HLH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 19]:
            min_v[28 * s_i + 19] = min_tmp
        if max_tmp > max_v[28 * s_i + 19]:
            max_v[28 * s_i + 19] = max_tmp

        tmp = anchor4_sequeeze(HLH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 20]:
            min_v[28 * s_i + 20] = min_tmp
        if max_tmp > max_v[28 * s_i + 20]:
            max_v[28 * s_i + 20] = max_tmp

        tmp = anchor1_sequeeze(LHH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 21]:
            min_v[28 * s_i + 21] = min_tmp
        if max_tmp > max_v[28 * s_i + 21]:
            max_v[28 * s_i + 21] = max_tmp

        tmp = anchor2_sequeeze(LHH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 22]:
            min_v[28 * s_i + 22] = min_tmp
        if max_tmp > max_v[28 * s_i + 22]:
            max_v[28 * s_i + 22] = max_tmp

        tmp = anchor3_sequeeze(LHH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 23]:
            min_v[28 * s_i + 23] = min_tmp
        if max_tmp > max_v[28 * s_i + 23]:
            max_v[28 * s_i + 23] = max_tmp

        tmp = anchor4_sequeeze(LHH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 24]:
            min_v[28 * s_i + 24] = min_tmp
        if max_tmp > max_v[28 * s_i + 24]:
            max_v[28 * s_i + 24] = max_tmp

        tmp = anchor1_sequeeze(HHH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 25]:
            min_v[28 * s_i + 25] = min_tmp
        if max_tmp > max_v[28 * s_i + 25]:
            max_v[28 * s_i + 25] = max_tmp

        tmp = anchor2_sequeeze(HHH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 26]:
            min_v[28 * s_i + 26] = min_tmp
        if max_tmp > max_v[28 * s_i + 26]:
            max_v[28 * s_i + 26] = max_tmp

        tmp = anchor3_sequeeze(HHH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 27]:
            min_v[28 * s_i + 27] = min_tmp
        if max_tmp > max_v[28 * s_i + 27]:
            max_v[28 * s_i + 27] = max_tmp

        tmp = anchor4_sequeeze(HHH_list[s_i])
        tmp = tmp[0, 0, :, :, :]
        min_tmp = torch.min(tmp).item()
        max_tmp = torch.max(tmp).item()
        if min_tmp < min_v[28 * s_i + 28]:
            min_v[28 * s_i + 28] = min_tmp
        if max_tmp > max_v[28 * s_i + 28]:
            max_v[28 * s_i + 28] = max_tmp

    min_v = (np.array(min_v)).astype(np.int32)
    max_v = (np.array(max_v)).astype(np.int32)
    return min_v, max_v

