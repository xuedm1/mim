import copy
import os
import pdb
import time

import numpy as np
import torch
from PIL import Image
from torch.autograd import Variable
from torch.nn import functional as F

# import arithmetic_coding as ac
import Model_ct_lossy as Model_ct
from utils import find_min_and_max_4, img2patch, img2patch_padding, img2patch_padding_ct
import SimpleITK as sitk

import pdb
import rans

PRECISION = 16
FACTOR = torch.tensor(2, dtype=torch.float32).pow_(PRECISION)


def to_variable(x):
    if torch.cuda.is_available():
        x = x.cuda()
    return Variable(x)


def write_binary(ans_enc, value, bin_num):
    bin_v = '{0:b}'.format(value).zfill(bin_num)
    quantized_cdf = [0, 32768, 65536]
    for i in range(bin_num):
        ans_enc.encode_cdf(int(bin_v[i]), quantized_cdf)


def load_nii(path):
    nii = sitk.ReadImage(path)
    nii = sitk.GetArrayFromImage(nii)
    return nii


def save_nii(img, path):
    img = sitk.GetImageFromArray(img)
    sitk.WriteImage(img, path)


def anchor1_sequeeze(y):
    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H // 2, W // 2], device=y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 0::2, 0::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 1::2, 1::2]

    return anchor


def anchor2_sequeeze(y):
    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H // 2, W // 2], device=y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 1::2, 1::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 0::2, 0::2]

    return anchor


def anchor3_sequeeze(y):
    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H // 2, W // 2], device=y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 0::2, 1::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 1::2, 0::2]

    return anchor


def anchor4_sequeeze(y):
    B, C, Z, H, W = y.shape
    anchor = torch.zeros([B, C, Z, H // 2, W // 2], device=y.device)
    anchor[:, :, 0::2, :, :] = y[:, :, 0::2, 1::2, 0::2]
    anchor[:, :, 1::2, :, :] = y[:, :, 1::2, 0::2, 1::2]

    return anchor


def de_anchor_unsequeeze1(anchor):
    B, C, Z, H, W = anchor.shape
    y_anchor = torch.zeros([B, C, Z, H * 2, W * 2], device=anchor.device)
    y_anchor[:, :, 0::2, 0::2, 0::2] = anchor[:, :, 0::2, :, :]
    y_anchor[:, :, 1::2, 1::2, 1::2] = anchor[:, :, 1::2, :, :]

    return y_anchor


def de_anchor_unsequeeze2(anchor):
    B, C, Z, H, W = anchor.shape
    y_anchor = torch.zeros([B, C, Z, H * 2, W * 2], device=anchor.device)
    y_anchor[:, :, 0::2, 1::2, 1::2] = anchor[:, :, 0::2, :, :]
    y_anchor[:, :, 1::2, 0::2, 0::2] = anchor[:, :, 1::2, :, :]

    return y_anchor


def de_anchor_unsequeeze3(anchor):
    B, C, Z, H, W = anchor.shape
    y_anchor = torch.zeros([B, C, Z, H * 2, W * 2], device=anchor.device)
    y_anchor[:, :, 0::2, 0::2, 1::2] = anchor[:, :, 0::2, :, :]
    y_anchor[:, :, 1::2, 1::2, 0::2] = anchor[:, :, 1::2, :, :]

    return y_anchor


def de_anchor_unsequeeze4(anchor):
    B, C, Z, H, W = anchor.shape
    y_anchor = torch.zeros([B, C, Z, H * 2, W * 2], device=anchor.device)
    y_anchor[:, :, 0::2, 1::2, 0::2] = anchor[:, :, 0::2, :, :]
    y_anchor[:, :, 1::2, 0::2, 1::2] = anchor[:, :, 1::2, :, :]

    return y_anchor


def anchor_mask(y):
    y[:, :, 0::2, 1::2, 0::2] = 1
    y[:, :, 0::2, 0::2, 1::2] = 2
    y[:, :, 1::2, 1::2, 1::2] = 3
    y[:, :, 1::2, 0::2, 0::2] = 4

    return y


def concat1(x):
    out = torch.cat((anchor1_sequeeze(x), anchor2_sequeeze(x)), 1)
    return out


def concat2(x):
    out = torch.cat((anchor1_sequeeze(x), anchor2_sequeeze(x), anchor3_sequeeze(x)), 1)
    return out


def concat3(x):
    out = torch.cat((anchor1_sequeeze(x), anchor2_sequeeze(x), anchor3_sequeeze(x), anchor4_sequeeze(x)), 1)
    return out


def concat_(x1, x2):
    out = torch.cat((x1, x2), 1)
    return out


def concat1_(x1, x2, x3):
    out = torch.cat((x1, x2, x3), 1)
    return out


def Recon(x_l, x_h, x_s):
    reconstructed_data = ((x_h << 8) | x_l) * x_s

    return reconstructed_data


def enc_lossy(args):
    if not os.path.exists(args.bin_dir):
        os.makedirs(args.bin_dir)
    if not os.path.exists(args.log_dir):
        os.makedirs(args.log_dir)
    if not os.path.exists(args.recon_dir):
        os.makedirs(args.recon_dir)

    bin_name = args.img_name[0:-4]
    logfile = open(args.log_dir + '/enc_log_' + bin_name + '.txt', 'a')

    ans_enc = rans.RansEncoder()
    write_binary(ans_enc, args.isLossless, 1)

    trans_steps = 4

    with torch.no_grad():

        model_path = args.model_path
        print(model_path)
        checkpoint = torch.load(model_path)

        all_part_dict = checkpoint['state_dict']

        models_dict = {}
        models_dict['transform'] = Model_ct.Transform()
        models_dict['entropy_LLL_1'] = Model_ct.CodingLLL_1()
        models_dict['entropy_LLL_2'] = Model_ct.CodingLLL_2()
        models_dict['entropy_LLL_3'] = Model_ct.CodingLLL_3()
        models_dict['entropy_LLL_4'] = Model_ct.CodingLLL_4()

        models_dict['entropy_HLL_1'] = Model_ct.CodingHLL_1()
        models_dict['entropy_HLL_2'] = Model_ct.CodingHLL_2()
        models_dict['entropy_HLL_3'] = Model_ct.CodingHLL_3()
        models_dict['entropy_HLL_4'] = Model_ct.CodingHLL_4()

        models_dict['entropy_LHL_1'] = Model_ct.CodingLHL_1()
        models_dict['entropy_LHL_2'] = Model_ct.CodingLHL_2()
        models_dict['entropy_LHL_3'] = Model_ct.CodingLHL_3()
        models_dict['entropy_LHL_4'] = Model_ct.CodingLHL_4()

        models_dict['entropy_HHL_1'] = Model_ct.CodingHHL_1()
        models_dict['entropy_HHL_2'] = Model_ct.CodingHHL_2()
        models_dict['entropy_HHL_3'] = Model_ct.CodingHHL_3()
        models_dict['entropy_HHL_4'] = Model_ct.CodingHHL_4()

        models_dict['entropy_LLH_1'] = Model_ct.CodingLLH_1()
        models_dict['entropy_LLH_2'] = Model_ct.CodingLLH_2()
        models_dict['entropy_LLH_3'] = Model_ct.CodingLLH_3()
        models_dict['entropy_LLH_4'] = Model_ct.CodingLLH_4()

        models_dict['entropy_HLH_1'] = Model_ct.CodingHLH_1()
        models_dict['entropy_HLH_2'] = Model_ct.CodingHLH_2()
        models_dict['entropy_HLH_3'] = Model_ct.CodingHLH_3()
        models_dict['entropy_HLH_4'] = Model_ct.CodingHLH_4()

        models_dict['entropy_LHH_1'] = Model_ct.CodingLHH_1()
        models_dict['entropy_LHH_2'] = Model_ct.CodingLHH_2()
        models_dict['entropy_LHH_3'] = Model_ct.CodingLHH_3()
        models_dict['entropy_LHH_4'] = Model_ct.CodingLHH_4()

        models_dict['entropy_HHH_1'] = Model_ct.CodingHHH_1()
        models_dict['entropy_HHH_2'] = Model_ct.CodingHHH_2()
        models_dict['entropy_HHH_3'] = Model_ct.CodingHHH_3()
        models_dict['entropy_HHH_4'] = Model_ct.CodingHHH_4()
        models_dict['post'] = Model_ct.Post()

        models_dict_update = {}
        for key, model in models_dict.items():

            myparams_dict = model.state_dict()
            # print(myparams_dict.keys())
            all_part_dict_new = {}
            # print(checkpoint.keys())
            for k1, v1 in all_part_dict.items():
                new_k = k1.replace('module.', '') if 'module.' in k1 else k1
                all_part_dict_new[new_k] = v1
            # print(all_part_dict_new.keys())
            part_dict = {k: v for k, v in all_part_dict_new.items() if k in myparams_dict}
            # print(part_dict.keys())
            myparams_dict.update(part_dict)
            model.load_state_dict(myparams_dict)
            if torch.cuda.is_available():
                model = model.cuda()
                model.eval()
            models_dict_update[key] = model
        models_dict.update(models_dict_update)

        print('Load pre-trained model succeed!')
        logfile.write('Load pre-trained model succeed!' + '\n')
        logfile.flush()

        img_path = args.input_dir + '/' + args.img_name

        start = time.time()

        print(img_path)
        logfile.write(img_path + '\n')
        logfile.flush()

        img = load_nii(img_path)
        img = np.array(img, dtype=np.float32)
        original_img = copy.deepcopy(img)

        img = torch.from_numpy(img)
        img = img.unsqueeze(0)
        img = img.unsqueeze(1)

        size = img.size()
        depth = size[2]
        height = size[3]
        width = size[4]

        write_binary(ans_enc, depth, 15)
        write_binary(ans_enc, height, 15)
        write_binary(ans_enc, width, 15)

        pad_z = int(np.ceil(depth / 16)) * 16 - depth
        pad_h = int(np.ceil(height / 32)) * 32 - height
        pad_w = int(np.ceil(width / 32)) * 32 - width
        paddings = (0, pad_w, 0, pad_h, 0, pad_z)
        img = F.pad(img, paddings, 'constant')
        print("img_shape", img.shape)

        input_img_v = to_variable(img)

        LLL, HLL_list, LHL_list, HHL_list, LLH_list, HLH_list, LHH_list, HHH_list, LLL_list_h, HLL_list_h, LHL_list_h, HHL_list_h, LLH_list_h, HLH_list_h, LHH_list_h, HHH_list_h, LLL_list_s, HLL_list_s, LHL_list_s, HHL_list_s, LLH_list_s, HLH_list_s, LHH_list_s, HHH_list_s = \
        models_dict['transform'].forward_trans(input_img_v, args.init_scale)

        min_v, max_v = find_min_and_max_4(LLL, HLL_list, LHL_list, HHL_list, LLH_list, HLH_list, LHH_list, HHH_list)
        minmin = np.min(min_v)
        maxmax = np.max(max_v)

        for j in range(116):
            tmp = min_v[j] + 8000
            write_binary(ans_enc, tmp, 15)
            tmp = max_v[j] + 8000
            write_binary(ans_enc, tmp, 15)

        # coded_coe_num = 0

        # high generator

        # feature_high = models_dict['high_generator'](input_img_high)

        TEST_time1 = 0.  # total time of entropy coding
        TEST_time2 = 0.  # total time of building context
        TEST_time3 = 0.  # total time of predicting probs
        TEST_time4 = 0.  # total time of probs round and normalize
        TEST_time5 = 0.  # total time of rans engine

        paddings = (2, 2, 2, 2, 0, 0)
        ref_padding = torch.nn.ReplicationPad3d((2, 2, 2, 2, 0, 0))

        for i in range(1, 5):

            TEST_time_temp1 = time.time()
            TEST_time_temp2 = time.time()
            if i == 1:  # compress LLL_1
                a = min_v[0]
                b = max_v[0] + 1
                enc_oth = anchor1_sequeeze(LLL)
                enc_oth = img2patch(enc_oth, 1, 1, 1, 1, 1, 1)
                enc_oth = F.pad(enc_oth, paddings, "constant")
                context = concat_(anchor1_sequeeze(LLL_list_h[3]), anchor1_sequeeze(LLL_list_s[3]))

            elif i == 2:  # compress LLL_2
                a = min_v[113]
                b = max_v[113] + 1
                enc_oth = anchor2_sequeeze(LLL)
                enc_oth = img2patch(enc_oth, 1, 1, 1, 1, 1, 1)
                enc_oth = F.pad(enc_oth, paddings, "constant")
                context = concat1_(anchor1_sequeeze(LLL), anchor2_sequeeze(LLL_list_h[3]),
                                   anchor2_sequeeze(LLL_list_s[3]))

            elif i == 3:  # compress LLL_3
                a = min_v[114]
                b = max_v[114] + 1
                enc_oth = anchor3_sequeeze(LLL)
                enc_oth = img2patch(enc_oth, 1, 1, 1, 1, 1, 1)
                enc_oth = F.pad(enc_oth, paddings, "constant")
                context = concat1_(concat1(LLL), anchor3_sequeeze(LLL_list_h[3]), anchor3_sequeeze(LLL_list_s[3]))

            elif i == 4:  # compress LLL_4
                a = min_v[115]
                b = max_v[115] + 1
                enc_oth = anchor4_sequeeze(LLL)
                enc_oth = img2patch(enc_oth, 1, 1, 1, 1, 1, 1)
                enc_oth = F.pad(enc_oth, paddings, "constant")
                context = concat1_(concat2(LLL), anchor4_sequeeze(LLL_list_h[3]), anchor4_sequeeze(LLL_list_s[3]))

            context = ref_padding(context)
            context = img2patch_padding_ct(context, 1, 5, 5, 1, 1, 1, 0, 2, 2)
            TEST_time2 += time.time() - TEST_time_temp2

            cur_context = context
            TEST_time_temp2 = time.time()
            prob = models_dict['entropy_LLL_' + str(i)].inf_cdf_lower(cur_context, a, b)
            TEST_time3 += time.time() - TEST_time_temp2
            index = (enc_oth[:, 0, 0, 0 + 2, 0 + 2] - a).cpu().data.numpy().astype(np.int32)

            # convert to int and normalize
            TEST_time_temp2 = time.time()
            factor = FACTOR.to(prob.device)
            Lp = prob.shape[-1]
            new_max_value = factor + 1 - Lp
            add_tensor = torch.arange(Lp, dtype=torch.int32, device=prob.device).repeat(prob.shape[0], 1)
            prob = prob.mul(new_max_value).add(add_tensor).round().cpu().numpy().astype('int32')
            TEST_time4 += time.time() - TEST_time_temp2

            TEST_time_temp2 = time.time()
            ans_enc.encode_cdf_batch(index, prob)
            TEST_time5 += time.time() - TEST_time_temp2
            TEST_time1 += time.time() - TEST_time_temp1
            print('LLL_' + str(i) + ' encoded...')

        sub_list = ['HLL', 'LHL', 'HHL', 'LLH', 'HLH', 'LHH', 'HHH']

        for ind in range(trans_steps):

            j = trans_steps - 1 - ind

            if args.is_CT == 1:

                ##### CT

                if j == 3:
                    code_block_size_z = 1
                    code_block_size_x = 4
                    code_block_size_y = 2

                    stride_z = 1
                    stride_x = 4
                    stride_y = 2

                elif j == 2:
                    code_block_size_z = 2
                    code_block_size_x = 2
                    code_block_size_y = 4

                    stride_z = 2
                    stride_x = 2
                    stride_y = 4

                elif j == 1:
                    code_block_size_z = 4
                    code_block_size_x = 4
                    code_block_size_y = 8

                    stride_z = 4
                    stride_x = 4
                    stride_y = 8

                else:
                    code_block_size_z = 4
                    code_block_size_x = 16
                    code_block_size_y = 8

                    stride_z = 4
                    stride_x = 16
                    stride_y = 8

            else:

                #### MRI

                if j == 3:
                    code_block_size_z = 1
                    code_block_size_x = 1
                    code_block_size_y = 2

                    stride_z = 1
                    stride_x = 1
                    stride_y = 2

                elif j == 2:
                    code_block_size_z = 1
                    code_block_size_x = 7
                    code_block_size_y = 1

                    stride_z = 1
                    stride_x = 7
                    stride_y = 1

                elif j == 1:
                    code_block_size_z = 11
                    code_block_size_x = 7
                    code_block_size_y = 1

                    stride_z = 11
                    stride_x = 7
                    stride_y = 1

                else:
                    code_block_size_z = 11
                    code_block_size_x = 7
                    code_block_size_y = 2

                    stride_z = 11
                    stride_x = 7
                    stride_y = 2

            for i in range(1, 29):

                TEST_time_temp1 = time.time()
                TEST_time_temp2 = time.time()
                if i == 1:  # compress HLL_1
                    enc_oth = img2patch(anchor1_sequeeze(HLL_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat(
                        (concat3(LLL), anchor1_sequeeze(HLL_list_h[j]), anchor1_sequeeze(HLL_list_s[j])), 1)

                elif i == 2:  # compress HLL_2
                    enc_oth = img2patch(anchor2_sequeeze(HLL_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), anchor1_sequeeze(HLL_list[j]), anchor2_sequeeze(HLL_list_h[j]),
                                         anchor2_sequeeze(HLL_list_s[j])), 1)

                elif i == 3:  # compress HLL_3
                    enc_oth = img2patch(anchor3_sequeeze(HLL_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat1(HLL_list[j]), anchor3_sequeeze(HLL_list_h[j]),
                                         anchor3_sequeeze(HLL_list_s[j])), 1)

                elif i == 4:  # compress HLL_4
                    enc_oth = img2patch(anchor4_sequeeze(HLL_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat2(HLL_list[j]), anchor4_sequeeze(HLL_list_h[j]),
                                         anchor4_sequeeze(HLL_list_s[j])), 1)

                elif i == 5:  # compress LHL_1
                    enc_oth = img2patch(anchor1_sequeeze(LHL_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), anchor1_sequeeze(LHL_list_h[j]),
                                         anchor1_sequeeze(LHL_list_s[j])), 1)

                elif i == 6:  # compress LHL_2
                    enc_oth = img2patch(anchor2_sequeeze(LHL_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), anchor1_sequeeze(LHL_list[j]),
                                         anchor2_sequeeze(LHL_list_h[j]), anchor2_sequeeze(LHL_list_s[j])), 1)

                elif i == 7:  # compress LHL_3
                    enc_oth = img2patch(anchor3_sequeeze(LHL_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat1(LHL_list[j]),
                                         anchor3_sequeeze(LHL_list_h[j]), anchor3_sequeeze(LHL_list_s[j])), 1)

                elif i == 8:  # compress LHL_4
                    enc_oth = img2patch(anchor4_sequeeze(LHL_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat2(LHL_list[j]),
                                         anchor4_sequeeze(LHL_list_h[j]), anchor4_sequeeze(LHL_list_s[j])), 1)

                elif i == 9:  # compress HHL_1
                    enc_oth = img2patch(anchor1_sequeeze(HHL_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]),
                                         anchor1_sequeeze(HHL_list_h[j]), anchor1_sequeeze(HHL_list_s[j])), 1)

                elif i == 10:  # compress HHL_2
                    enc_oth = img2patch(anchor2_sequeeze(HHL_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]),
                                         anchor1_sequeeze(HHL_list[j]), anchor2_sequeeze(HHL_list_h[j]),
                                         anchor2_sequeeze(HHL_list_s[j])), 1)

                elif i == 11:  # compress HHL_3
                    enc_oth = img2patch(anchor3_sequeeze(HHL_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat1(HHL_list[j]),
                                         anchor3_sequeeze(HHL_list_h[j]), anchor3_sequeeze(HHL_list_s[j])), 1)

                elif i == 12:  # compress HHL_4
                    enc_oth = img2patch(anchor4_sequeeze(HHL_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat2(HHL_list[j]),
                                         anchor4_sequeeze(HHL_list_h[j]), anchor4_sequeeze(HHL_list_s[j])), 1)

                elif i == 13:  # compress LLH_1
                    enc_oth = img2patch(anchor1_sequeeze(LLH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         anchor1_sequeeze(LLH_list_h[j]), anchor1_sequeeze(LLH_list_s[j])), 1)

                elif i == 14:  # compress LLH_2
                    enc_oth = img2patch(anchor2_sequeeze(LLH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         anchor1_sequeeze(LLH_list[j]), anchor2_sequeeze(LLH_list_h[j]),
                                         anchor2_sequeeze(LLH_list_s[j])), 1)

                elif i == 15:  # compress LLH_3
                    enc_oth = img2patch(anchor3_sequeeze(LLH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         concat1(LLH_list[j]), anchor3_sequeeze(LLH_list_h[j]),
                                         anchor3_sequeeze(LLH_list_s[j])), 1)

                elif i == 16:  # compress LLH_4
                    enc_oth = img2patch(anchor4_sequeeze(LLH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         concat2(LLH_list[j]), anchor4_sequeeze(LLH_list_h[j]),
                                         anchor4_sequeeze(LLH_list_s[j])), 1)

                elif i == 17:  # compress HLH_1
                    enc_oth = img2patch(anchor1_sequeeze(HLH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         concat3(LLH_list[j]), anchor1_sequeeze(HLH_list_h[j]),
                                         anchor1_sequeeze(HLH_list_s[j])), 1)

                elif i == 18:  # compress HLH_2
                    enc_oth = img2patch(anchor2_sequeeze(HLH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         concat3(LLH_list[j]), anchor1_sequeeze(HLH_list[j]),
                                         anchor2_sequeeze(HLH_list_h[j]), anchor2_sequeeze(HLH_list_s[j])), 1)

                elif i == 19:  # compress HLH_3
                    enc_oth = img2patch(anchor3_sequeeze(HLH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         concat3(LLH_list[j]), concat1(HLH_list[j]), anchor3_sequeeze(HLH_list_h[j]),
                                         anchor3_sequeeze(HLH_list_s[j])), 1)

                elif i == 20:  # compress HLH_4
                    enc_oth = img2patch(anchor4_sequeeze(HLH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         concat3(LLH_list[j]), concat2(HLH_list[j]), anchor4_sequeeze(HLH_list_h[j]),
                                         anchor4_sequeeze(HLH_list_s[j])), 1)

                elif i == 21:  # compress LHH_1
                    enc_oth = img2patch(anchor1_sequeeze(LHH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         concat3(LLH_list[j]),
                                         concat3(HLH_list[j]), anchor1_sequeeze(LHH_list_h[j]),
                                         anchor1_sequeeze(LHH_list_s[j])), 1)

                elif i == 22:  # compress LHH_2
                    enc_oth = img2patch(anchor2_sequeeze(LHH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         concat3(LLH_list[j]), concat3(HLH_list[j]), anchor1_sequeeze(
                        LHH_list[j]), anchor2_sequeeze(LHH_list_h[j]), anchor2_sequeeze(LHH_list_s[j])), 1)

                elif i == 23:  # compress LHH_3
                    enc_oth = img2patch(anchor3_sequeeze(LHH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         concat3(LLH_list[j]), concat3(HLH_list[j]),
                                         concat1(LHH_list[j]), anchor3_sequeeze(LHH_list_h[j]),
                                         anchor3_sequeeze(LHH_list_s[j])), 1)

                elif i == 24:  # compress LHH_4
                    enc_oth = img2patch(anchor4_sequeeze(LHH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         concat3(LLH_list[j]), concat3(HLH_list[j]),
                                         concat2(LHH_list[j]), anchor4_sequeeze(LHH_list_h[j]),
                                         anchor4_sequeeze(LHH_list_s[j])), 1)

                elif i == 25:  # compress HHH_1
                    enc_oth = img2patch(anchor1_sequeeze(HHH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         concat3(LLH_list[j]), concat3(HLH_list[j]),
                                         concat3(LHH_list[j]), anchor1_sequeeze(HHH_list_h[j]),
                                         anchor1_sequeeze(HHH_list_s[j])), 1)

                elif i == 26:  # compress HHH_2
                    enc_oth = img2patch(anchor2_sequeeze(HHH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         concat3(LLH_list[j]),
                                         concat3(HLH_list[j]), concat3(LHH_list[j]), anchor1_sequeeze(HHH_list[j]),
                                         anchor2_sequeeze(HHH_list_h[j]), anchor2_sequeeze(HHH_list_s[j])), 1)

                elif i == 27:  # compress HHH_3
                    enc_oth = img2patch(anchor3_sequeeze(HHH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         concat3(LLH_list[j]), concat3(HLH_list[j]), concat3(LHH_list[j]),
                                         concat1(HHH_list[j]), anchor3_sequeeze(HHH_list_h[j]),
                                         anchor3_sequeeze(HHH_list_s[j])), 1)

                elif i == 28:  # compress HHH_4
                    enc_oth = img2patch(anchor4_sequeeze(HHH_list[j]), code_block_size_z, code_block_size_x,
                                        code_block_size_y, stride_z, stride_x, stride_y)
                    enc_oth = F.pad(enc_oth, paddings, "constant")
                    context = torch.cat((concat3(LLL), concat3(HLL_list[j]), concat3(LHL_list[j]), concat3(HHL_list[j]),
                                         concat3(LLH_list[j]), concat3(HLH_list[j]), concat3(LHH_list[j]),
                                         concat2(HHH_list[j]), anchor4_sequeeze(HHH_list_h[j]),
                                         anchor4_sequeeze(HHH_list_s[j])), 1)

                context = ref_padding(context)
                context = img2patch_padding_ct(context, code_block_size_z, code_block_size_x + 4, code_block_size_y + 4,
                                               stride_z, stride_x, stride_y, 0, 2, 2)
                TEST_time2 += time.time() - TEST_time_temp2

                x = 28 * j + i
                a = min_v[x]
                b = max_v[x] + 1
                temp_add_tensor = 1
                for z_i in range(code_block_size_z):
                    for h_i in range(code_block_size_x):
                        for w_i in range(code_block_size_y):

                            cur_context = context[:, :, z_i:z_i + 1, h_i:h_i + 5, w_i:w_i + 5]
                            TEST_time_temp2 = time.time()
                            prob = models_dict[
                                'entropy_' + sub_list[(i - 1) // 4] + '_' + str((i - 1) % 4 + 1)].inf_cdf_lower(
                                cur_context, a, b, j)
                            TEST_time3 += time.time() - TEST_time_temp2
                            index = (enc_oth[:, 0, z_i, h_i + 2, w_i + 2] - a).cpu().data.numpy().astype(np.int32)

                            # convert to int and normalize
                            TEST_time_temp2 = time.time()
                            if temp_add_tensor == 1:
                                temp_add_tensor = 0
                                factor = FACTOR.to(prob.device)
                                Lp = prob.shape[-1]
                                new_max_value = factor + 1 - Lp
                                add_tensor = torch.arange(Lp, dtype=torch.int32, device=prob.device).repeat(
                                    prob.shape[0], 1)
                            prob = prob.mul(new_max_value).add(add_tensor).round().cpu().numpy().astype('int32')
                            TEST_time4 += time.time() - TEST_time_temp2

                            TEST_time_temp2 = time.time()
                            ans_enc.encode_cdf_batch(index, prob)
                            TEST_time5 += time.time() - TEST_time_temp2

                TEST_time1 += time.time() - TEST_time_temp1

                print(sub_list[(i - 1) // 4] + '_' + str((i - 1) % 4 + 1) + str(j) + ' encoded...')

            # pdb.set_trace()
            if ind == 0:
            # LLL recon & dequant
                LLL = Recon(LLL.type(torch.int32), LLL_list_h[j], LLL_list_s[j])
                LLL = LLL.type(torch.float32) * args.init_scale

            HLL_list[j] = Recon(HLL_list[j].type(torch.int32), HLL_list_h[j], HLL_list_s[j])
            LHL_list[j] = Recon(LHL_list[j].type(torch.int32), LHL_list_h[j], LHL_list_s[j])
            HHL_list[j] = Recon(HHL_list[j].type(torch.int32), HHL_list_h[j], HHL_list_s[j])
            LLH_list[j] = Recon(LLH_list[j].type(torch.int32), LLH_list_h[j], LLH_list_s[j])
            HLH_list[j] = Recon(HLH_list[j].type(torch.int32), HLH_list_h[j], HLH_list_s[j])
            LHH_list[j] = Recon(LHH_list[j].type(torch.int32), LHH_list_h[j], LHH_list_s[j])
            HHH_list[j] = Recon(HHH_list[j].type(torch.int32), HHH_list_h[j], HHH_list_s[j])

            HLL_list[j] = HLL_list[j].type(torch.float32) * args.init_scale
            LHL_list[j] = LHL_list[j].type(torch.float32) * args.init_scale
            HHL_list[j] = HHL_list[j].type(torch.float32) * args.init_scale
            LLH_list[j] = LLH_list[j].type(torch.float32) * args.init_scale
            HLH_list[j] = HLH_list[j].type(torch.float32) * args.init_scale
            LHH_list[j] = LHH_list[j].type(torch.float32) * args.init_scale
            HHH_list[j] = HHH_list[j].type(torch.float32) * args.init_scale

            LLL = models_dict['transform'].inverse_trans(LLL, HLL_list[j], LHL_list[j], HHL_list[j], LLH_list[j],
                                                         HLH_list[j], LHH_list[j], HHH_list[j])

        LLL = models_dict['post'].forward(LLL.to(torch.float32))
        LLL = torch.clamp(torch.round(LLL), min=-32768, max=32767)
        recon = LLL[0, 0, 0:depth, 0:height, 0:width]

        print('encoding finished!')
        logfile.write('encoding finished!' + '\n')
        end = time.time()
        print('Encoding-time: ', end - start)
        logfile.write('Encoding-time: ' + str(end - start) + '\n')

        print(TEST_time1, TEST_time2, TEST_time3, TEST_time4, TEST_time5)

        rv = ans_enc.flush()
        print('total bytes:', len(rv))
        with open(args.bin_dir + '/' + bin_name + '.bin', "wb") as f:
            f.write(rv)

        print('bit_out closed!')
        logfile.write('bit_out closed!' + '\n')

        filesize = len(rv) * 8 / height / width / depth
        print('BPP: ', filesize)
        logfile.write('BPP: ' + str(filesize) + '\n')
        logfile.flush()

        recon = recon.cpu().data.numpy().astype(np.float32)

        mse = np.mean((recon - original_img) ** 2)
        psnr = (10. * np.log10(65535. * 65535. / mse)).item()

        print('PSNR: ', psnr)
        logfile.write('PSNR: ' + str(psnr) + '\n')
        logfile.flush()

        save_nii(recon, args.recon_dir + '/' + bin_name + '.nii')

    logfile.close()
